# Pasadena Chronicle

> Are you a writer? Head to [pasadenachronicle.org/editor/](https://pasadenachronicle.org/editor/)

This repository holds the next version of the Pasadena Chronicle website, made with Hugo. Using Hugo simplifies the total complexity by containing the articles and website in one repository and outputting a static site. Writers can now use NetlifyCMS to edit articles through a easier interface.