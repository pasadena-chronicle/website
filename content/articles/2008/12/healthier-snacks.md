---
title: Exchanging Gummy Worms For Pirate’s Booty
subtitle: Student Store profits decline
writers:
- Grace Persico
date: 2008-12-01
image: 
caption: 
topics:
- food
---

On the first day of school, many students dutifully lined up outside the Student Business Office at lunchtime, a seemingly familiar ritual at Pasadena High School. However, the Student Business Office was closed. A few days later, posters appeared on the walls around the school (courtesy of ASB) announcing that the Student Business Office would re-open in a few weeks complete with a new menu. The new menu featured healthier alternatives that included Pirate’s Booty and baked chips. But why the change for the place where students used to be able to pick up a bag of hot fries and gummy worms? “There was too much fat content. We were not in compliance with the rules, ow there are only a few foods we can sell,” says Ms. Costa, who runs the Student Store. She’s referring to a law passed in 2001 that says no more than 35% of the calories present in snacks sold in public schools can be from fat. The bill was aimed at unhealthy students whom many legislators believe will clog our healthcare systems when they age because they receive such unhealthy food both in and out of school.

While in theory, the law sounds great and it really does have the nation’s best interest at heart, it has hurt sales immensely. “There’s been a big decline in profit, almost half. One day we would make $400 and now we’re making around $200 a day,” Costa says. Not only have the new sales hurt the school, but it has also put pressure on the clubs who sell at the Student Business Office. 

However, while the snacks being sold now might not be the fat-laden snacks students are used to, they’re still delicious and certainly much better for the student’s overall health.

<!-- Source: https://pasadenachronicle.wordpress.com/2009/03/10/exchanging-gummy-worms-for-pirates-booty/ -->
