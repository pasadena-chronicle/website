---
title: PHS Boys Water Polo Update
subtitle: Renewed interest shows in new recruits
writers:
- George Gallagher
date: 2017-09-01
image: phs-pool.jpg 
caption: The PHS pool | Source www.pusd.us
topics:
- sport
---

The season is well underway for boys water team, and this year’s team is as strong as ever. One of the challenges that the team has managed to overcome is recruiting new players. The junior varsity team is larger than it has been in a few years due to an inﬂux of freshman, so the team captain, sophomore Levi Mualem, definitely has his work cut out for him. He is a great leader who already has a lot of useful leadership experience from being a member of Boy Scout Troop 110 based out of Sierra Madre. He is very optimistic in his hopes for the season, as he works towards improving the physical level of the players as well as teaching them the strategies they can implement during games. So far the team has been unsuccessful in games, but they practice very hard and are determined to do as best as they can with the resources they are provided.

The varsity team has no shortage of fresh players as well due to the outgoing class of 2017 having composed a very large portion of the team. One of the newest members of the varsity team is sophomore Colin Juett, who deﬁnitely has much to offer the team. He has a lot of experience with aquatic sports as a six-year member of the sports club Swim Pasadena. This experience makes him a very fast swimmer, and as a sophomore, he still has lots of time to improve his water polo expertise. The captains of the varsity team are seniors Owen Brandi-kit, a two-year member of varsity, and Caleb Vance, who is a three-year member. Both of these players have spent time at the renowned Rose Bowl Aquatics Center, where they participated in the water polo club. Varsity has been working very hard with the leadership of their captains and the experience of their coaches.

Pasadena High School’s boys water polo team still has a lot of games left in the season, so we look forward to seeing them grow as a team and have a successful season. Come to any of the team‘s games and show your school spirit! Go Bulldogs!
