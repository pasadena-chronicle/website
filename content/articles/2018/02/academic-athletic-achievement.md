---
title: Academic & Athletic Achievement at PHS
subtitle: Lilit Yengoian & Tavian Percy
writers:
- Savannah Bradley
date: 2018-02-01
# image: 
# caption: 
topics:
- student
---

These two students are being recognized for their academic and athletic excellence.

First, there’s “Lily” Lilit Yengoian, who has a weighted Academic GPA of 4.7. She also was honored in the top 10 of the Class of 2018, placed second in math field day, made it on the 4.0 and 4.5 honor roll, and received the president’s award for educational excellence. Lily is not only academically inclined, but is also athletic, for she is on Varsity Tennis and was awarded the Pasadena High School Tennis Award.

Q: Although it may be a little early to ask, do you feel that all that hard work and effort was worth it?
A: “...I think all the hard work definitely was worth it because it made me resilient and taught me how to manage my time well.”

Q: What would you like to do as a career?
A: “I really want to become a neurologist, so I can not only research new methods of treating neurological disorders but also treat them.”

Q: Who has been your biggest inspiration?”
A: “My biggest inspiration has been Logic, because he grew up poor, in a family of drug and alcohol addicts, but he found his passion and worked his butt off to get where he is today!”

Our other outstanding student is our new varsity basketball star Tavian Percy, who moved here from Florida. This 6’5” senior has been playing basketball since the age of four, sports jersey #4, and is the starting shooting guard. In the team’s first six games, he averaged 12 points a game, 6.3 rebounds, and 2.5 assists.

Q: What has your experience on the basketball team/at PHS been like so far?
A: “My experience has been fun and funny with the basketball team this year. We’re like brothers, so we hang out with each other and play around with each other so I would say it’s been a pretty good experience so far.”

Q: If you could play for any NBA team which would you choose?
A: “I would choose the Lakers so I could stay close and experience LA more”

Q: What’s your favorite thing about basketball?
A: “My favorite thing about basketball is that there is a chance you could make it your job and have fun with it.”
