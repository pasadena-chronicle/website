---
title: District Drowning in Debt
subtitle: Where did it come from?
writers:
- Rosie Gongora
date: 2018-02-01
image: wilson.webp
caption: At the end of the school day, Joseph Mendoza, 12, and his classmates leave Wilson Middle School in Pasadena on Tuesday, Feb. 27, 2018. Wilson is one of five schools that PUSD may close to fill a budget gap. | Photo by Sarah Reingewirtz, Pasadena Star-News/SCNG
topics:
- district
---

Whether it has come up in one of your classes, or maybe you read about it somewhere online or in the newspaper, you have most likely heard about the gaping 20 million dollar debt that the Pasadena Unified School District is in. The matter has become such an issue that if by March the PUSD does not have their reserve back to the state-mandated 3 percent, the state is going to have to intervene.

Many of you are probably wondering, how did this large debt come about? Well, Scott Phelps, a district board member and the chair of the financial committee, posted “Declining enrollment and decreasing state funding point to a coming fiscal emergency in the PUSD” in the Pasadena Weekly to explain the reasons as to why the debt has accumulated to the large amount that it has. The first reason Phelps stated was, “Unrestricted revenue and special education revenue have actually decreased over the last two years due to declining enrollment and the lack of significant increases in state funding...” The second reason was, “...the 6-percent raise granted to all staff in the spring of 2016.” The final reason was, “... state-mandated retirement contribution increases and increased health care market cost [for teachers].” These three reasons are why the district’s “budget is not doing well.”

After broaching the topic with the superintendent and the United Teacher of Pasadena’s president, both declined to answer, having no official statement about the debt. Our principal Mr. Hernandez said that though there may be changes because of cutbacks, he is “...trying to keep consistency for the next school year.” As of right now, it is a waiting game to see whether or not the district is going to be able to avoid having a state overseer come in.

Just something more to learn. 
