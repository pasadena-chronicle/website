---
title: DIY Valentine’s Day Crafts
subtitle: Make gifts for your loved ones
writers:
- Suha Kim
date: 2018-02-01
image: hands-with-jar.webp
caption: 'Person making a "why I love you" jar | Credit www.ellieellie.co.uk'
topics:
- holiday
---

Looking for a DIY gift that your loved ones will actually want? These pretty projects could not be more perfect for Valentine’s Day.

1. A jar of reasons why you love them 
   Decorate a small jar with ribbons, felt, and other decorative items. Wrap the jar with twine with a button in the middle. The button holds a heart shaped felt cut-out in place. Write reasons why you love and appreciate that person on small pieces of folded paper and put them in the jar. 

2. Personalized photo frame
   Create a photo frame that you can’t buy anywhere else. You can use paint, stickers, masking tape, ribbons, etc. Let your creativity go wild!

![Example](hands-with-jar.webp)
