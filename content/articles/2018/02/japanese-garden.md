---
title: Storrler Stearns Japanese Garden
subtitle: A hidden gem of Pasadena 
writers:
- Trisha Valmocena
date: 2018-02-01
image: teahouse.jpg
caption: A building over water at Storrler Stearns Japanese Garden. | Credit www.discovernikkei.org
topics:
- local
---

The serenity of the garden is undeniable. A tea house hung over one of the ponds while a performer plays the koto. Koi ﬁshes swam around in dazzling patterns that many children found tantalizing as they put their hands into the water. The plants, although of several types, blended well and created a picturesque scene. Today, the Storrier Stearns Japanese Garden is owned by Jim and Connie Haddad. There are two areas to buy authentic items such as antique poems, porcelain containers, and even traditional Japanese clothing. Jim works by the gallery and he mentioned, “We have so much art here.”'as unhung paintings are stacked up in the corner. Meanwhile, Connie operates the clothing store where many kimonos can be seen among the origami swans.

The history of the garden is as fascinating as the garden itself. Charles and Ellamae Storrier Stearns made several trips to Japan; and in 1935, they hired the expert Kinzuchi Fujii to design their garden. It took seven years to make, even with Fujii’s expertise in carpentry and landscaping. Unfortunately, with the outbreak of the second World War, he was sent to an internment camp in 1942. The Stearns died, and in 1950 their estate was auctioned. Gamelia Poulsen, the mother of Jim Haddad, made a successful bid. The mansion was dis- mantled for reuse elsewhere and most of the estate was gradually sold off, only the garden and enough property for a house were kept. In 1976, the Caltrans acquired an easternmost piece of the property. Due to the anticipated construction of the 710 Freeway, another chunk was taken. Mrs. Poulsen let the garden go and sold some of the artifacts. Mysterious- ly in 1981, the tea house burned down.

Takeo Uesugi took an interest in the garden in 2005, recognizing the cultural and historical significance of the garden. He, therefore, volunteered to oversee the restoration, and the tea house was rebuilt. The Storrier Stearns Japanese Garden is now a California Historical Landmark and a California Register of Historic Places.
