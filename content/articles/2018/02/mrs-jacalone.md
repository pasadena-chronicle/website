---
title: Who is Mrs. Jacalone?
subtitle: A soccer coach and AP biology teacher
writers:
- Rosie Gongora
date: 2018-02-01
topics:
- teacher
---

Rachael Jacalone is a regular and AP biology teacher here at PHS. She is also the assistant coach for the girl’s soccer team. She grew up in Monrovia and attended Monrovia High School. Mrs. Jacalone had knee surgery her senior year in high school, which inspired her to want to help people and work as a physical therapist after attending Loyola Marymount University. But during an internship in a physical therapy office, she realized that she didn’t like it as much as she thought she would. She admitted, “I wanted to work specifically with high-school age athletes, and at the PT [physical therapy] office I had to work with all ages.” This prompted her to become a high school teacher.

Mrs. Jacalone began her teaching career at PHS in 2011, so she’s now a 7th-year veteran. She says, “My favorite part about teaching is my students. I love to see their minds working, and when they understand content...it’s a good feeling!” As a biologist, “I love that there are always new scientific discoveries and ideas to explore.” Mrs. Jacalone is an amazing teacher, and we are beyond grateful to have her here at PHS.
