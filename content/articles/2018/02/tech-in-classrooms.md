---
title: 'Technology in Our Classrooms: Chromebooks'
subtitle: What are they and why are they taking over the eductation market?
writers:
- Sophie Manoukian
date: 2018-02-01
image: 31-acer-chromebook-714.webp
caption: An Acer Chromebook 714. | Photo by Sarah Tew/CNET
topics:
- district
- technology
---

Earlier this month, the freshmen, sophomores, and juniors received information about the newest educational advancement at Pasadena High School - personal Chromebooks. As my younger peers begin to experience constant technology in the classroom through Chromebooks, I am inclined to ask myself: what exactly is a Chromebook and why are so many elementary, middle, and high schools replacing their hardcover textbooks and paper-based exams and homework assignments with what looks like the average laptop?

A Chromebook is a laptop that runs on a Linux-based Chrome OS system that stores most of its applications and data in the cloud rather than on the machine. Linux is a family of free and open-source software operating systems that are built around the Linux Kernel: an open-source computer program that has complete control over everything in the system. The Chromebook is Linux-based because its primary operating system is the Chrome OS, a system designed by Google that uses the Google Chrome web browser as its principal user interface. The Chromebook is universally appealing due to its cloud connection and storage and easy-to-use Google Chrome applications. Schools across the nation have advocated for Chromebooks for students of all ages, and by 2012 schools had become the most significant Chromebook consumer.

Why exactly are these devices so prominent in the education sector? Beyond the benefits of the cloud and the popular Google Chrome user interface, teachers and administrations favor the Chromebook because of Google’s free and useful educational applications such as word processing, spreadsheets, and presentations. The price of the Chromebook is also a reason why school districts are choosing this device over other options (like the iPad for instance): the price per Chromebook is an average $220, which is an affordable option for many school’s buying in bulk. This low price not only saves schools money but also allows the younger children in the school district to receive Chromebooks at an earlier age.

Other positive changes the Chromebook transition can bring include the benefit of easy-to-access online resources such as dictionaries, thesauruses, and world news, the reduction of paper use due to online tests and assignments, and allowing younger students to practice their typing skills through consistent Chromebook use.

Some concerns with the advancement of the Chromebook’s prominence in schools include the possible increase in cheating and distractions, and that the strict blockage of many sites on the Chromebook limit freedom while also preventing research.

No matter how you feel about Chromebooks, if you’re a freshman, sophomore, or junior at Pasadena High School, Chromebooks and technology are going to become an integral part of not only your education but also the education of the students who come after you.
