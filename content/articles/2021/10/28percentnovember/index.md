---
title: Morgan Gaskell - A Cool Woman
subtitle: From the 28% Newsletter
writers:
- Mallika Sheshadri
date: 2021-10-13
topics:
- student
- teacher
- club
image: mgaskell.jpg  
caption: Morgan Gaskell (10)
---

The COVID-19 pandemic was an impetus for many people to take up various hobbies. Morgan Gaskell, a sophomore at PHS, took this time to explore the depths of ornithology, the study of birds. “I’ve always been a big wildlife enthusiast,” she said to me. “When I moved to my home in Southern California, wildlife was everywhere! I was so lucky to spot wildlife such as the American black bears and California mule deer wandering through my yard” When quarantine began, she found herself turning to the most abundant wildlife in her yard﹣ birds. “I put up two bird feeders and the birds came!” she said, “Quarantine really became a time for me to learn about my local species.” 

What impressed me the most about Morgan was her knowledge and passion for the subject. She has her own website called the "News for the Dedicated Zoologist", where she writes a bi-monthly newsletter about her discoveries and experiences. “I created News for the Dedicated Zoologist in February of 2018. I was in 6th grade at the time and had a wonderful math and science teacher who sparked my interest in STEM. I also had a fantastic English teacher who inspired me to share my writing.” 

Last May, Morgan joined a bird banding group made up of individuals interested in ornithology from all over the LA area. “Every other week, I’m waking up at 3:30 am to get to Zuma by sunrise. For the next six hours, we’re catching birds in nets and processing them– taking measurements, determining sex and age from feathers, etc. Watching birds is one thing, but holding them in your hand lets you learn about the bird’s life.” she wrote in "News for the Dedicated Zoologist." 

Her website also includes documentation of birds she’s seen and nests she’s tracked in the last few years. Her publications are detailed and interesting, complete with phenomenal photography of her own experiences. Morgan Gaskell is truly a treasure to the world of STEM and biology.

Read for on the 28% Newsletter!
