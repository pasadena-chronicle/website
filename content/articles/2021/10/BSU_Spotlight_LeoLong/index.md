---
title: The Black Student Union
subtitle: Forgers of Unity and Community 
writers:
- Leo Long
date: 2021-10-13
topics:
- student
- teacher
- local
- club
- politics
- community
thumbnail: BSU.jpg  
caption: The BSU's cabinet and advisors. | Photo by Leo Long
---

On September 28th, The Chronicle had the pleasure of interviewing the Black Student Union cabinet, including President Trinity Taylor, Vice President Kennedy Harmon, Secretary Zoe Williams as well as Advisor Ms. Cauley, to gain their insights as to what BSU is about and what we can learn from their mission! What came through loud and clear is that the core of BSU is community engagement, diversity, and unity. As Taylor explained, it is her enduring hope that the club is seen by students, especially African-American youths, as an opportunity to come together and work towards common goals. Williams and Kennedy were sure to denote, as well, that the Black Student Union is a place for all, and seeks to rally students from all backgrounds behind a shared message for the promotion of equity and building relationships. 

When asked how they practically go about manifesting said ideals, the cabinet enumerated various traditions BSU partakes in. For instance, every year the club participates in what is known as the Brotherhood Assembly, wherein groups on campus dedicated to a particular culture come together to share and admire their cultural traditions and practices, and celebrate the beauty of diversity! Also among the list of pertinent activities discussed is the College Expo, a yearly tradition in which Black youths take a field trip to various historic African-American Universities and do in-person interviews. When further inquired, the group revealed that not only has the Expo been a great opportunity to get young kids thinking about their futures, but the success rate has been staggering, as according to the cabinet, all seniors who attended this last expo were accepted! 

Lastly was a question of what BSU wanted to say to the whole of the PHS community, and the response was truly inspiring. Trinity made it apparent that there was a concern around representation of Black students in various community activities, with student government being brought up as a prime example. This is not to be mistaken for a message of failings on part of individual students, she explained, but rather an example of how years of discrimination and oppression have fostered a culture that discourages Black individuals from pursuing activities based in community leadership. 

So how might we go about addressing this issue? Well in the words of Advisory and APP Academy teacher Ms. Cauley, the answer is simple: education, and specifically, education reform. She explained how an issue that permeates modern curriculum is how African-American history is taught, noting the fact that many students are unaware of the sheer amount of Black neighborhoods that have been razed in order to suppress African-Americans, and that such distortions of fact then shield us from an important reality around the history of race in America. She, and the whole of the cabinet, argued that it is only by truly understanding the extent of this oppression will then the community be motivated to see that equity is achieved, and those Black students who feel marginalized or disenfranchised will feel empowered to seek out community, and take back their voice. 

BSU will always be a place for students to come together in harmony and build bonds, and so, it seems, will it be an agent of change, however this depends on the whole of the community being committed to that core ideal of unity, for in that will strength come forth, and dreams made reality!
