---
title: 'Historical Spotlight: Pasadena City Hall'
subtitle: "The history behind it's making"
writers:
- Nolan Romero
date: 2021-10-15 
topics:
- local
- history
---

Located on 100 North Garfield Avenue, right in heart of our city, Pasadena City Hall first started construction in the late 1920s. In 1923, the city approved a bond issuing $3.5 million for the creation of a civic center. Around $1.3 million went towards the making of the city hall, the rest being used towards the making of the Central Library and the Civic Auditorium. To today's cash value, $3.5 million in 1923 is estimated to be around $57 million. Standing at 6 stories tall - with highest point being 206 feet above ground level - Pasadena City Hall remains an impressive structure, even today. Pasadena City Hall includes 235 different rooms and covers 170,000 square feet.

The building's construction was completed on December 27, 1927. Pasadena City Hall was designed by architects John Bakewell and Arthur Brown. Although they both studied at University of California, Berkeley, the duo met at National School of Fine Arts in Paris, France. The pair designed many other city halls, such as Berkely City Hall and San Francisco City Hall. Their architectural influences derived from the style of Italian early Renaissance architect Andrea Palladio. Palladio was responsible for 23 different structures around Italy, the most well-known being the Villa Almerico-Capra.

The beauty and size of the city hall, along side with the whole city, has attracted lots of attention from tourists and has brought inspiration to many film directors. All around Pasadena, you many find films or shows being shot. Even right behind Pasadena High School, at the abandoned St. Luke's Medical Center, there always seems to be someone shooting something. Pasadena City hall has been featured in all kinds of movies and films, Parks and Recreation being the most well-known television example and Charlie Chaplin's Oscar-nominated 1940 film "The Great Dictator."
 
Although Pasadena existed long before City Hall was built, the City Hall truly marks the beginning of a contemporary Pasadena as it has evolved into the city it is today.
