---
title: Devious Licks
subtitle: A Dangerous, Destructive Trend
writers:
- Jesus Gonzalez
date: 2021-10-15  #date when you want this to appear on website (at the soonest)
topics:
- local
- school
---
Unless you have been living under a rock, you have most likely heard of TikTok trend of committing “Devious licks”. You might be wondering what exactly a “Devious lick” is - the answer had no correlation to what the name suggests. A “Devious lick’ is an action in which a student will steal school property and show it off. As crazy as it sounds, students all throughout the world are committing these normalized acts of theft, stealing stuff from as little as toilet paper to as big as alleged kidnapping of school faculty and even our school has fallen victim to this new trend.

A good starting place to understanding how this trend started is finding its origin. After a few hours of scrolling through the #deviouslick (a hashtag that has since been blocked for violating community guidelines) page of TikTok, I found the oldest video - dating back to August 21, 2021. The post showcased a kid stealing the sink from his school bathroom. Things escalated quickly, with every user on TikTok trying to one-up each other. The next significant video was posted on August 27, 2021, in which the user filmed himself stealing one of the buses from his school. Although this video was taken down within the first day, it nonetheless left a big impact. The video in question was one-upped within days, with a user posting a video in which he allegedly claimed to have kidnapped his teacher as part of the trend. The information in the video was never verified as true or false. Despite this being the peak of how illegal the trend would get, it was from over as soon after came a series of copy cat videos that would last the rest of the month and not die till near the end of September.

The trend had a relatively long lifetime, lasting around a month and not having an exact end. Based upon post frequencies on Tiktok, the prominence of the trend roughly spanned from September 26 to September 30. As I had previously mentioned, our school also got affected, with three hand sanitizers dispensers being stolen and one being vandalized during this time period. This trend really emphasized the power that social media exerts on us - compelling thousands of students across the country to steal school property for virtual clout. The “Devious lick” trend will probably fall into obscurity within a few months, but it will at least have a long lasting economic impact on schools.
