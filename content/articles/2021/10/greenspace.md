---
title: Where did all the plants go? 
subtitle: A history of greenspace at PHS (and why we don't have any)
writers:
- Polly McConnell
date: 2021-10-15
topics:
- campus
- nature
- environmental
---

On average, each student at PHS walks through the quad and canteen five or six times a day. On top of that, most make two trips to the locker rooms, and one more across campus while exiting at the final bell. This evens out to roughly eight or nine trips around campus throughout the day. That’s eight or nine times passing the barren rectangles of dirt that sit in the center of our school. 

While it’s clear that these empty lots were originally designed to be strips of greenspace across the campus, today they’re anything but green. Take the large expanse of land between the gyms, for example: dirt, concrete, litter, and not a plant in sight. 

“It just seemed very… dry,” said Gianna Gullon, when asked to describe her first impression of PHS’s campus. Gianna is a freshman at PHS this year. Over the course of the short two months that we’ve been in school, Gianna has already vocalized her concerns about the campus greenspace to me numerous times. Given her interest in the topic, I eagerly sat down to interview her at lunch one day over plastic-wrapped burritos and legal pads. “If there were plants, they weren’t that green, and they were dying,” she explained, furrowing her brow at the barren dirt lot behind us. 

There are approximately four of these spaces around the campus, five or six if you’re counting those at the front of the school. One between the gyms, one on either side of the E building, and one in the center quad. Students can’t go a day at school without passing each of them at least once. 

Given how prominent these spaces are on campus, I found it hard to believe that PHS’s landscaper designed them solely for the purpose of holding dirt. There had to be some garden, field, or even structure there before.

So if not empty lots, what were these spaces? And what happened to them? 

These burning questions set me on a pursuit to uncover the history of the dirt lots. I knew I couldn’t be the only student curious about these spaces.

After asking around for a couple days, I found myself talking to Mr. Michealsen, who many students know for teaching AP Human Geography and AP European History. Perhaps less known, he is also the staff supervisor for PHS’s very own Green Club, one of the more prominent and long-standing groups on campus. Green Club, according to Mr. Michealsen, has been around for about a decade now. It first began with the mission to implement recycling in the school, and has since carried on to focus on campus beautification, off-site cleanups, and climate education. 

Michealsen, who’s been teaching at PHS for sixteen years, was able to offer me some insight about these empty lots. “All that space was grass,” he said. “There were sprinkler systems set up, and it was green. It was all green.”

After hearing this, I took the time to dig around for some old pictures of PHS. It was difficult at first to sort through what was and wasn’t our campus, but after a minute, I found some dated photos of it from 1963. And Mr. Michealsen was right: these empty lots that we pass by every day were filled with grass. 

This led me to my next question: where did the grass go? 

“I think two things happened,” he explained. “One, we had some drought years, and there was more regulation about water usage. And second, our whole irrigation system at the school is super dated. It’s the original 1960’s irrigation, so much of it doesn’t work anymore.”

As quite the nature enthusiast myself, I happen to know that grass isn’t the most friendly plant when it comes to water efficiency. In fact, it’s notoriously difficult to grow and maintain in Southern California, where we’re almost always in a drought and our weather is above 80 degrees most of the year. So that made me wonder: if grass was so tedious, why not just plant native plants which are already adapted to Pasadena’s climate? 

Turns out that Green Club actually tried filling these lots with native plants, back about eight years ago. But that didn’t work out, either. 

“We took that recycling money and started planting,” Mr. Michealsen recalled. That year, Green Club, teamed with financial assistance from the district and the PTSA, planted about 400 plants across PHS’s campus. They even recruited the help of a landscape architect to map out what are called desire paths (the off-road, unofficial trails that humans create by cutting through areas for efficiency’s sake). 

But again, the plan fell through. Plants died. The ground dried out. And now, here we are in 2021 with empty dirt lots once more. 

The death of over 300 plants, according to Mr. Michealsen, was fault of the practically ancient irrigation systems. Green Club thought they could rely on the sprinklers to keep the native plants alive, but when they came back from summer break, it was a graveyard. “The understanding was that it’d get irrigated… and it didn’t.” 

You can see the survivors of this project on campus today, in the space in front of the E building. They’re struggling, and many appear dried out, but it’s certainly a start. I was curious as to how these plants were alive, considering that there’s no sprinklers set up for them - so I tracked down the two students at the heart of the issue. 

Today, Green Club is run by the dynamic duo Isa Eisenberg and Lindsay Frieberger. Both seniors, they’ve seen the campus through a variety of stages. They’ve already kicked off the year with a number of cleanups and multiple garden days (the latter of which takes place behind the small theatre in PHS’s garden). 

“If we want the campus to look nice, it’s kind of our job to do it,” Lindsay explained. Both leaders are discouraged by the current state of the irrigation systems, but are doing everything they can in spite of it to keep the remaining plants alive. 

Isa drew it back to planting a native landscape: “Even if it doesn't look too nice right now, it’s better than having grass,” she said. “Grass sucks up a lot of water, and it doesn't produce a lot of benefits for the animals.” She said that the City of Pasadena will actually pay you to get rid of your grass. 

We were standing outside G208, looking down into the center quad as we spoke. There’s a nice patch of semi-native plants, including salvia, toyon, and what appears to be orange trees right there. This, I thought, was a good example of a possible design for other empty spaces on campus. “When we were sophomores, the only job we had was to water that,” Lindsay recalled with a laugh. 

Currently, there are about ten Green Club students that volunteer to water the surviving plants twice a week… but it doesn’t seem to be enough, and it certainly wouldn’t be if we planted on a larger scale. 

“The issue right now is, if you plant stuff, you have to water it by hand. And unless you’re coming in during the summer, everything’s gonna die,” Mr. Michealsen explained. 

The other solution that we discussed would be redoing the irrigation systems with modern technology. While this would definitely work, it gets tricky on a financial level. 

Roberto Hernandez, school principal, affirmed that this would be a five-digit operation. Tens of thousands of dollars is a lot of money - more than the school has felt comfortable with investing in campus beautification so far. 

“I think with what we’ve been through, and with what we’re going through, there are larger priorities,” Principal Hernandez said, in reference to the pandemic. He’s right, too - families all across the world are struggling with even getting their kids an education right now, and it is absolutely in the right for PHS to make student support their top priority at the moment. 

“It’s hard to invest right now when there’s no long-term solution or plan in place,” he added. According to him, there was a grand plan - much like the one Michealsen referenced - to landscape the areas between and around the gyms. This project, called “Measure TT” was well on its way a couple years ago (leading up to the pandemic). 

Nobody’s certain about why the plan fell through, but Principal Hernandez thinks it’s possible that the operation was put on hold as the school encountered the bizarre new challenges of COVID-19.

It doesn’t mean, however, that the plan rolled completely to a stop. “I didn’t want anyone to forget,” he said. So, starting this summer and since school’s been back, our principal has contacted the district about this issue multiple times. He hopes to get the landscaping back on track as soon as possible. 

“We need a permanent solution,” he concluded. “It just makes the campus look so much better.” 

But… what if implementing greenspace across PHS could do more than make it look better? What if the benefits are deeper than aesthetics?

I talked to Linney Parra, freshman class president, about this issue. She had an interesting take on the idea: “Green gives oxygen, right? I didn’t really feel, well, the oxygen in the air. I feel like with more green, people would like campus more,” she reasoned. “They’d want to sit around. Hang out.” She pointed out that especially during the pandemic, we should really be spaced out and breathing clean air during lunchtime. She thinks that greenspace could provide a place for people to sit - with distance - and enjoy their lunch outside. 

“Greenspaces not only make the campus look better, they can actually help someone’s mental health,” Linney told me. “I know a lot of people who say, ‘oh, yeah, going out in nature helps me relax.’ And I think that… helping a few people is better than helping no one, you know?” 

These magical health benefits of nature that Linney references aren’t just theory. Hundreds of studies have repeatedly demonstrated the clear positive impact that greenspace has on mental health. One 2019 study done by the Danish Civil Registration System found that children who grew up around reduced amounts of nature had a 55% increased risk of developing a handful of psychiatric disorders when they matured (including depression, substance abuse, eating disorders, and more). And greenspace doesn’t only lessen the chance of these disorders - it provides clear positive rewards for students, as well. According to the European World Health Organization, greenspaces alleviate stress and promote relaxation and social opportunity. They can also restore attention span and improve ADHD symptoms, two things that are incredibly beneficial to students. 

In 2016, Harvard University went so far as to initiate the Plant Project, which aimed to educate students on the positive effects of greenspace. The program also highlighted the inequalities that many face in terms of access to nature, like in inner cities and other urbanized residential areas. 

In Pasadena, we are fortunate enough to have many parks, hiking trails, and botanical gardens (not to mention the San Gabriel Mountains right behind us). At a quick glance, this alone may seem like enough exposure to greenspace. However, when you take into consideration the fact that students are spending at least seven hours at school five days a week, it’s clear that PHS’s campus is one of the main landscapes that students are exposed to. It’s also important to note that teenage years are some of the most crucial in terms of development, so what high schoolers ‘soak in’ now will likely impact us for the rest of our lives on a psychological level. For these reasons, a green campus could be instrumental in shaping students’ education, mental health, and developmental patterns in the long run. 

As I was talking to another freshman, Jadyn Addicott, she brought up something that I hadn’t yet considered: we could design this greenspace to be more than… well… greenspace. What if it could serve the students directly? 

“There’s nowhere to hang out,” Jadyn pointed out. Currently, she eats lunch every day near the center quad at some tables, but she’s not so enthusiastic about the cement atmosphere of it. “If there were more trees and more greenspace, I feel like it’d be more inviting. It would encourage people to sit down and hang out in the grass, and if there were trees, there’d be shade for us.” 

Jadyn proposed placing large rocks in these spaces for people to take a seat on amongst the plants. We could even revive the ideas that Green Club had a few years ago: carve out desire paths, or walkways, that cut through these spaces to help people get to class easier. These are just two examples of the many ways that this greenspace could potentially accommodate the students. 

More than anything, as I did my research, I got the sense that renovating these areas could be a real source of pride for the school. 

During my interview with Principal Hernandez, I remember him referencing something called the “Broken Window Theory”. Basically, the theory states that when things appear to be in poor condition (such as a broken window, or in our case a not-so-green campus), it’ll create an atmosphere that promotes even more destruction and neglect. 

The principal believes that the converse of this is also true: for PHS, a greener campus would result in a cleaner campus. The leaders of Green Club highlighted this idea as well: “If we could get more students involved, and caring, it will definitely make a difference in how the campus looks as a whole,” Isa said. “How people treat it… and how people treat each other.”

We could truly come together as a community over a project like this - and the end result would be something that we were proud of. Something that prospective families who were looking at PHS would see and think, ‘Wow, this is really wonderful. I want my child to be a part of this.’ 

“I always think of this school as a focal point of the community,” Principal Hernandez explained. “Yes, it’s appealing to the eye, but there’s a sense of pride as well.”

It’s easy to picture exactly what he’s talking about: the graduates, families of graduates, and staff of PHS form a well-bound Bulldog community in Pasadena. It’s a school rich with tradition, spirit, and stories. And when you factor in the foot traffic and many eyes that the school receives because of football games, Victory Park, and the farmer’s market... it is truly a central aspect of Pasadena’s culture. There’s every reason to add to that pride by innovating the campus. 
“It’s a responsibility,” Green Club cited. 

So yes, there are vast financial hurdles that need to be overcome. Yes, it will likely take work from everyone to get a project like this going, and yes, the results won’t be instant. 

But in the end, almost everyone I talked to agreed that the benefits of having greenspace will infinitely outweigh the costs. This is something that could innovate the campus, boost our community, and advantage students and their mental health. 

The students are the voices and center of the public school system. Green Club is right: if we want a greener campus, we are the ones who have to make it happen. It’s up to us to bring this to the attention of the district. 

Write letters. Talk to your teachers about it. Make calls. Raise awareness. We have the power in our hands to make PHS a truly beautiful and healthy place: to reflect the world we want to live in. 







