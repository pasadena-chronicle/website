---
title: Early Humans Even Earlier?
subtitle: "The first Americans could actually have arrived as early as 20,000 years ago"
writers:
- Charis Graham 
date: 2021-10-20  #date when you want this to appear on website (at the soonest)
topics:
- science
- archeology
- ancience
- humans
---

In the White Sands National Park in Southern New Mexico, new evidence, reported in Nature magazine and originally published September 23 in Science Magazine, has been found to suggest that the first Americans could actually have arrived as early as 20,000 years ago. For a long time, archaeologists believed that the earliest Americans came somewhere between 11,000 and 13,000 years ago. At the end of the last ice age, sea levels were lower, exposing a land bridge from Siberia to Alaska. Early human hunters, following their prey, were believed to have come from Siberia into America.

However, new evidence, in the form of human footprints suggest otherwise. These footprints, dated to be 21,000 to 23,000 years old, are now the oldest credible proof of human activity in the Americas. Of course, there had been other evidence similar to this that suggested earlier human activity, but none as strong. Other claims, including one that suggests people were in California as early as 130,000 years ago, have been disregarded as the product of natural rather than human interference. For example, what was thought to be human made tools were actually mistaken rocks.


This new evidence places humans here nearly 10,000 years before when they are commonly thought to have crossed into America. What’s even cooler is that these are thought to be the tracks of children and teenagers, those who are around our age. This evidence shows that there were people, just like PHS students, in America as long as 23,000 years ago.

