---
title: Leonardo the Skateboarding Robot
subtitle: A revolutionary robot created at Caltech
writers:
- Brianna Gaughan
date: 2021-10-20  #date when you want this to appear on website (at the soonest)
topics:
- science
- engineering
- robotics
- caltech
- drones
---

Recently, engineers at Caltech’s Center for Autonomous Systems and Technologies created a robot named Leonardo. Leo is a bipedal robot, meaning he has two legs and can walk. Well, walking isn’t all Leonardo can do, he can skateboard too. The robot’s unique design enables it to do a variety of activities that require advanced balance and coordination. Leo is essentially a drone on legs and combines flying and walking to achieve these high level activities. Inspired by the way birds hop and walk between power lines, Leo’s locomotion relies on coordination between propellers and thrusters. This revolutionary design enables the robot to effectively coordinate rough terrains, an issue that many other bipedal robots encounter. The next step for Leo is to reinforce his legs to support more weight and equip him with neural network software that enables him to learn from his environment. 

