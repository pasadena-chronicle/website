---
title: Looking Back in Time
subtitle: Modern telescopes allow us to view the history of the universe
writers:
- Brianna Gaughan
date: 2021-10-20  #date when you want this to appear on website (at the soonest)
topics:
- science
- astronomy
- telescope
- space
- galaxy
---

When you think of time travel, you think it’s only something you see in movies. It hasn’t been done and never will be. But, did you know we can look back in time? In the field of astronomy, many deep-space images are taken by a very famous telescope called the Hubble Space Telescope. Launched into space in 1990, HST has contributed greatly to our understanding of the universe, taking pictures of galaxies and objects lightyears away in space. But now, we are launching an even more power telescope: the James Webb Telescope. 

The James Webb Telescope, built by NASA, is currently the world’s most powerful telescope. It has yet to be launched and has experienced many delays, but is scheduled to go up on December 18. 2021. When it does, it will replace the Hubble Space Telescope as our main frontiersman. It is 100 times more powerful than Hubble and will be able to take pictures of objects from millions of years ago. You may be wondering, how can we take pictures of objects from a million years ago?

A camera takes a picture when it receives light. Depending on the distance the camera is away from the object you are imaging, it takes time for the light to travel to the camera lens. On Earth, it takes no time at all for light to travel to our eyes, cameras, and devices. Nothing can be faster than the speed of light. A “lightyear” is the distance light travels in one year, which is over a billion miles. The James Webb Telescope is able to image objects over 13 billion light years away. However, the light that the telescope is receiving took 13 billion years to reach the telescope, thus the image we see is from 13 billion years ago. Due to the sheer power of the James Webb Telescope, we will be able to see as far back as 100 million years after the Big Bang and will be able to see what our universe looked like when it was young, possibly uncovering secrets about the origin of our universe. 

