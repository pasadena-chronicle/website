---
title: The Stupid Nobel Prizes
subtitle: Prizes for the the more bizarre and silly endeavors out there
writers:
- Charis Graham 
date: 2021-10-20  #date when you want this to appear on website (at the soonest)
topics:
- nobel prize
- science
---

The Ig Nobel prizes are the opposite of the Nobel prizes. The Nobel prizes celebrate genuine scientific, artistic, and humanistic achievements. While the Ig Nobel also celebrates human achievements, they tend to reward the more bizarre and silly endeavors out there. For example, in this year’s 31st First Annual Ig Nobel Prize ceremony, held on September 9, the Biology Prize went to a group from Sweden who analyzed variations in the purrs, meowing, hissing and other sounds cats use to communicate with people. 

There are a wide variety of awards, spanning everything from economics to chemistry to transportation. The Economics prize went to Pavlo Blavatskyy who devised a way that the fatness of a country’s government officials could be a strong indicator of how corrupt that country is. The Chemistry prize went to a varied group with members from Germany, United Kingdom, New Zealand, Greece, Cyprus, and Austria. They chemically analyzed the air from inside movie theaters to test, according to the official Ig Nobel website, “whether the odors produced by an audience reliably indicate the levels of violence, sex, antisocial behavior, drug use, and bad language in the movie the audience is watching.” The transportation prize, probably one of the weirdest from the list, went to a group spawning from Tanzania, Zimbabwe, Brazil, United Kingdom, United States, South Africa, and Namibia. They experimented whether it was safer to transport rhinoceros upside-down when airborne. The lengthy name of the study was: “The Pulmonary and Metabolic Effects of Suspension by the Feet Compared with Lateral Recumbency in Immobilized Black Rhinoceroses (Diceros bicornis) Captured by Aerial Darting.”
Some of the awards even paired. The Kinetics prize went to a group from Japan, Switzerland, and Italy who tested why pedestrians sometimes do collide with each other. Comparatively, the Physics prize went to a group from the Netherlands, Italy, Taiwan, and the United States who tested why pedestrians sometimes don’t collide with each other.

However, the funniest prize by far was that for Peace. The Ig Nobel Peace prize, won by the United States, was awarded to Ethan Beseris, Steven Naleway, and David Career. They tested whether humans evolved beards to “protect themselves from punches to the face,” reports the official Ig Nobel website.



