---
title: Current Events
subtitle: Oil Spill off Coast of Huntington Beach
writers:
- Mallika Sheshadri 
date: 2021-10-13  #date when you want this to appear on website (at the soonest)
topics:
- current events
---

Something’s wrong when dead fish and birds begin to wash up on the shore of a beach, which is exactly what happened on Saturday, October 2nd. An oil pipeline leak off the coast of Huntington Beach in Orange County caused more than 120,000 gallons of crude to spill into the Pacific Ocean, extending all the way to Newport Beach.
 
Efforts are underway to clean the spill. By Sunday evening, 3,150 gallons of oil had been successfully removed from the ocean, according to an article in CNN. More than a mile of boom (floating buoys made for containing oil spills) were released in an attempt to isolate the crude. This is only a small fraction of the oil that had been released into the ocean, and experts determined that it will require millions of dollars to clean everything up.

The spill spans over 8,320 acres - more than half the size of Pasadena - killing wildlife and flowing into the Talbert Marsh, an ecological reserve in Huntington Beach home to dozens of species of birds. In an interview with the LA Times, Ben Smith, a biologist and environmental specialist in the area, said “If the birds get into this tar it’s going to stick to their feathers and it’s going to be a problem for them. It contaminated the water — it’s bad for the wildlife, bad for the water, bad for the people who use the water.” Despite the fact that the oil had spilled into the ocean, officials say that the animals most affected by the leak are birds.
The spill has also has negatively impacted coastal communities, as well. Officials were forced to cancel the third and final day of the annual Pacific Airshow, an event regularly attracting thousands of tourists from all over the nation, with a revenue of 71.5$ million dollars in 2019, according to the LA Times. The monumental costs associated with cleanup are devastating, amounting in a huge financial loss for the county. 

The spill is not the first to occur along the coast of Southern California. Past incidents have had a massive impact on the area, including the historic 1969 spill in Santa Barbara; a slick made of over 3 million gallons of oil. In recent years, environmentalists have made examples of these oil spills, reminding people of how important it is for California to switch over to renewable energy to prevent events like this from happening. 

Sources: 

Massive oil spill sends crude onto Orange County beaches, killing birds, marine life- Los Angeles Times- https://www.latimes.com/california/story/2021-10-02/coast-guard-rushes-to-contain-newport-beach-oil-slick

Major oil spill off coast of Southern California threatens shores from Huntington Beach to Laguna Beach- CNN- https://www.cnn.com/2021/10-04/us/california-oil-spill-monday/index.html
 
‘Major’ Oil Spill Off California Coast Threatens Wetlands and Wildlife- New York Times- https://www.nytimes.com/2021/10-03/us/pipeline-broken-oil-pacific-ocean.html


