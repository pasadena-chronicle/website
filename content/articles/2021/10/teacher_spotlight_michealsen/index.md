---
title: 'Teacher Spotlight: Mr. Michealsen'
subtitle: Featuring PHS's favorite AP Euro and AP Human Geography Teacher
writers:
- Jasmine Sov
- Gwendolyn Lopez
date: 2021-10-16
topics:
- teacher
thumbnail: mrmich.jpg
caption: Mr. Michealsen
---

As we walked into G208, the first thing we noticed were the many maps on the walls. What regions do they show, exactly? There’s one of Europe, of Europe during the Cold War, of the USSR, and not one, but two world maps. It’s exactly the place where you’d expect to find PHS’ Mr. Michealsen, our AP Human Geography and AP European History teacher.

Why did Mr. Michealsen start teaching? “I started teaching because I like working with youth. I think high school students are really creative, and I think they’re at a pivotal time in their life where they’re making a lot of decisions that are really important. The main reason I wanted a career in teaching was to help navigate that period of their life as they are growing up.” A history and international political science major from Seattle Pacific University, Mr. Michealsen has been teaching for 16 years and counting. A typical class of his usually involves lively discussions, diligent note-taking, and quite a bit of hollering and applause if you happen to walk in on a pre-test Jeopardy day.

“AP Euro has been really great,” says sophomore Morgan Gaskell. “Mr. Michealsen’s a great teacher, very engaging, helps us remember the material.”

As for AP Human Geography, freshman Paulina Mcconnell thinks it’s “a very important study that helps people be more aware and more connected to the world around them.” 

“I really like how interactive it is,” she adds. “That’s one of the best aspects of the class.”

Mr. Michealsen is also the advisor of the Green Club: a club that started over ten years ago with the goal of creating a more sustainable school community. “I had two students who were sophomores and said, ‘What’s happening to the paper on campus?’” Mr. Michealsen recounts. “I told them, ‘It’s getting thrown away.’ So they said, ‘We should recycle it’...It was just two guys initially, and it grew into a bigger thing.” And it certainly did—today - the Green Club is one of the most popular clubs on campus.

Curious about what has drawn Mr. Michealsen to PHS, we decided to ask about his favorite elements of our school. “My favorite thing about PHS is the history here—we’re a school that's been around for a long time. We have students from all different backgrounds... The culture at PHS is more tolerant and open because of the diversity that’s here. That’s a real strength of the school.” 

In addition to his teaching prowess, Mr. Michealsen is known for his formidable height. “I am six foot five,” he confirms, unwittingly disclosing some of the most hotly debated-over teacher trivia at PHS. It’s likely that, with the publication of this article, many bets have been won and lost.  

After we finish our interview and leave the classroom, we look back just before the door closes. Already, Mr. Michealsen has returned to his laptop, presumably preparing slideshows of European glory and demise, of humanity and its ever-changing environment. 
