---
title: Turkey Tussle Troubles
subtitle: Turkey Tussle Canceled due to Positive COVID-19 Test
writers:
- Tosten Pearson
date: 2021-10-27 
topics:
- student
- teacher
- club
- sport
- event
---

Our Gmail inboxes and school announcements hold unfortunate news - the annual Turkey Tussle has been canceled.

In a stark reminder of the continuing presence of COVID-19, the sudden cancellation owes itself to a participating athlete's positive COVID-19 test. Potential exposure to other students (both in and outside of the team) has created concerns regarding spectator safety at the Turkey Tussle. In accordance with Pasadena Unified School District, Pasadena Health Department, and CIF guidelines, the athletes will be quarantined, rendering the game tentatively postponed.

Efforts to reschedule the game are underway but face two main obstacles. Upcoming playoff games have constrained the Bulldog's schedule, leaving the Turkey Tussle with little time to occupy. Coupled with unavailability at the Rose Bowl, these factors have made rescheduling uncertain. The fate of the Victory Bell is yet to be known.

That is not to say, however, that the Tussle and its festivities have been abandoned entirely. As of the writing of this article, Homecoming activities are still set to take place, including our Spirit Week, Homecoming Assembly, and Homecoming Dance. 
	
The themes for this Spirit Week are Bring Everything But a Backpack and Jersey Day, Decades Day, Character Day, Muir Nerd Day, and Red/White and Senior Crowns Day. Classes will compete to earn points, with a cash prize awarded to the class with the greatest number of points.

Many Pasadena High School students will celebrate at the 2021 Homecoming Dance, set to take place on October 30th at Santa Anita Park. Tickets will be sold until October 22nd. The Homecoming Court is composed of seven princesses: Abigail Griffith, Olivia Hodges, Camila Morales Guevara, Daniela Sanchez, Trinity Taylor, Micaela Zamora, and Leila Zuniga. Out of these princesses, the Queen will be crowned during the school’s Homecoming Assembly towards the end of Spirit Week.

Let’s go Bulldogs!
