---
title: Closer To Normal
subtitle: COVID-19 Vaccines for Younger Children
writers:
- Nolan Romero
date: 2021-11-13
topics:
- local
- district
---

In the last couple of weeks, some good things have happened regarding the COVID-19 pandemic. On November 2, the Centers for Disease Control and Prevention announced the vaccination of children from the ages 5-11. The next day, many providers began offering the shot. Pfizer is the main source for these vaccines for those 11 and under. In these past weeks, already about 1 million children ages 5-11 have gotten their first COVID-19 vaccine shot. Currently, 60% of Americans are fully vaccinated, which is about 195 million people out of the total 329 American population. Data from 2019 reveals that out of 329 million, 73 million are under the age of 18 which makes up about 22% of the total population, allowing younger kids to have access to the vaccine helps increase the number of people vaccinated overall. It helps people stay protected and allows them to go to school, be with friends, and hopefully at some point, not have to wear masks anymore. With the holidays approaching, there will surely be a boost in COVID-19 cases. Our transition to normal life, whatever that may mean, will not be immediate, but we are nevertheless making progress.

