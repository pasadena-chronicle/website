---
title: "Club Spotlight: Art Club"
subtitle: All about one of the most creative places on campus
writers:
- Jasmine Sov
date: 2021-11-30
topics:
- student
- club
---

The Art Club, one of many new clubs introduced to PHS this year, is one that feels like it’s been here forever. “The Art Club was originally created to teach people who want to learn more about art,” says Nevada Cruz, club president. “Our club is pretty chaotic, but it’s pretty fun.”

“We’re here to help them develop their skills and answer any questions they might have,” adds Makenna Morrisey, the club’s vice president. “We teach newcomers to get into art and inspire artists to do projects and draw more.”

Every week or two, the Art Club will do either traditional or digital art based upon prompts centered around various aspects of art. At the time of this interview, they are currently doing prompts on character building and design. “Those who have more knowledge on it help those who need help. We take suggestions from other kids...one of our members wanted to learn how to build a character,” Nevada explains. At the end of each project, students will share their art and give constructive critique and comments on each other’s works. Essentially, it’s a system of mutual benefits—artists help other budding artists, allowing students to hone both their teaching and artistic skills. 

So what’s next on the Art Club’s agenda? “For the first month, we just did really fun projects, like doing special Halloween prompts...This month, we’re just going to start getting into character building or using different mediums,” says Nevada. What kind of mediums, exactly? “Some of us have used color pencils, some have used markers, and in the future I’d like to get more into charcoal or watercolors so that they (the members of the club) can include other mediums to make a really cool art piece. There’s a lot of combinations...you don’t have to just use one medium, you can use so many and it can be just beautiful in the end.”

Right now, the club is currently working out how to raise money for the club’s own art materials, as well as for possible visits to art museums in the future. One way that they plan to fundraise is through art commissions—the idea is that if people want a custom art piece, they can order one from the club. Of course, since the club is new, their main priorities right now are to bond, learn, and have fun.

With a promising future, the Art Club is sure to appeal to many students on campus. You can find them in N102 (the VADA room) on Tuesdays and Thursdays at lunch.

