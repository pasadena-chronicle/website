---
title: Toy Drive for Hospitalized Children
subtitle: PHS' newest club organizes toy drive for local hospital
writers:
- Charis Graham
date: 2021-11-12
topics:
- local
- club
- event
- student
---

Covid-19 has had major, lasting impacts on all aspects of society. Hospitals, especially, have been hit hard. Pasadena High School’s newest club - PHS in Support of Huntington Hospital - was created to help alleviate some of the strain at the local level. Nina Dinan, the founder of PHS in Support of Huntington Hospital, is a current senior and ASB Elections Commissioner who has a history of working with Huntington Hospital to organize the yearly blood drives. “Huntington Hospital is an essential part of our community,” Dinan said when asked for commentary, “and of course public health is more important than ever.” 
    
While the club is just beginning, it has already launched its first project: a toy drive for children at Huntington Hospital. Advertisements for the toy drive - soon to be posted and distributed - share that toys for the drive can be dropped off in D101 or at the front office. Suggested toys include coloring books, markers, Play-Doh, LEGOs, puzzles, books, and more. One thing they do ask for is for the items to be unopened and in the original packaging.

“This club is just beginning... but it has already brought together a wonderful group of students,” Dinan reports. “I hope to see it grow throughout the year and become a permanent part of our school.” Future projects in the works include campaigns to increase blood donation among PHS students.

Students hoping to get involved in PHS in Support of Huntington Hospital can join the club for meetings on alternate Thursdays at lunch in G205. “In the meantime,” Dinan declares, “drop off your toys at D101 and come join us at our next meeting!”


