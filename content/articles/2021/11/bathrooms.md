---
title: Toilet Taboo
subtitle: Campus-wide bathroom closure leaves students in a tailspin
writers:
- Polly McConnell
date: 2021-11-13
topics:
- student
- campus
---

When’s the last time you used the bathroom at PHS? Were you willing to suffer through walking across 3 buildings to reach the nearest open one, or was the grimy state of the sinks not worth the risk? 

It’s no secret that during this semester, bathrooms all across campus have been closed. This phenomenon is strange in the light of a recent pandemic, considering that many students are concerned about their sanitation and health during this time. 

Out of at least eight girls’ bathrooms on campus, only one or two are open on any given day. It’s the same situation with the boys: if you wanna use the bathroom, it’s a long and weary endeavor. Students express that it’s incredibly time consuming; teachers are frustrated with the several minutes it takes away from our education. 

One freshman described their experience with using the bathroom as an adventure. “My friends thought I left class for good,” they said. It’s almost comical: awkwardly half-jogging for minutes through countless hallways to find a bathroom, pulling desperately on the door handle, realizing it’s locked, and then repeating the process all over again. 

Seniors say this isn’t the first year that the bathrooms have been closed. In fact, there was a point around three years ago when not a single restroom outside the locker rooms was open. So, if they aren’t shut down for COVID precautions, what’s the deal? 

Most staff and students on campus agree that it boils down to two main issues. First off, the teenage vaping pandemic is noticeably present at our school, as it has been for years. Closure of the bathrooms seems to be the administration’s response to this issue: they reasoned that if students are just using restrooms to smoke, then the restrooms should be closed for all.

The second reason for mass closure is simply the ghastly state of our bathrooms. As is expected with any high school restroom, they’re grimy, smoky, vandalized, and reeking. Toilet paper and food wrappers are strewn across the floor, cigarette butts clog the sink drains, and gum sticks to the walls. It’s horribly unpleasant to venture into them, let alone for the school to have to clean them. 

Day after day passes and the bathrooms remain locked, perhaps pointing to long-term closure. Will they ever reopen? And when? Until then, students must continue to navigate this unusual crisis. 
