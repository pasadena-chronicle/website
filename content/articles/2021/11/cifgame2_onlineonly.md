---
title: Bulldog Football Victorious in Second CIF-SS Game 
subtitle: The team remains undefeated with a 41-31 victory.
writers:
- Tosten Pearson
date: 2021-11-16
topics:
- student
- sport
- event
---

After a long bus ride to Norwalk, California, the Pasadena Bulldogs played against the La Mirada High School Matadores in their quarterfinal game. Following the previous week's 50-13 victory against Thousand Oaks, the Bulldogs have progressed through the California Interscholastic Federation-Southern Section (CIF-SS), facing similarly successful teams from throughout Southern California. The return of wide receiver Mekhi Fox after a month of injury, a verified Bulldog star, added to the anticipation surrounding the game. 

The spectators certainly weren't disappointed.

Catching passes seemingly left and right, Fox scored a total of four touchdowns over the course of the game: two passing, one punt return, and one rushing touchdown. Quarterback Kaden Taylor scored one rushing touchdown and three passing touchdowns. From the outset, La Mirada was playing catch-up, eeking out one final touchdown in the last minutes of the fourth quarter.

Nevertheless, the effort was a mere dent in the Bulldogs' lead, with the final score being 41-31, the Bulldogs victorious.

The Bulldogs are set to play Aquinas Catholic High School on Friday, November 19th. The game will once again be an away game, taking place at Aquinas Catholic High School in San Bernadino, California. Such a distance may seem daunting, but the Bulldogs have certainly proven that a home-field advantage can be overcome with perseverance and skill.

Go Bulldogs!
