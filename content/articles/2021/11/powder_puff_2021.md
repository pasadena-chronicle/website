---
title: Powderpuff 2021
subtitle: Roles reverse in this exciting event!
writers:
- Mallika Sheshadri
date: 2021-11-19
topics:
- student
- local
- sport
- event  
---

A mere month ago, a buzz of excitement hummed amongst students as they waited in anticipation for the Homecoming activities that took place at the end of October. Even though the Homecoming festivities have come to an end (with the exception of the Turkey Tussle that has been postponed until further notice), the student population has refused to settle, and instead has set their sights on another event: the annual Senior vs. Junior Powderpuff.

With last year's game cancelled due to COVID-19 restrictions, this year's Powderpuff is highly anticipated. The classes of 2022 and 2023 are hard at work preparing for the game, with Varsity football and cheer training the teams. The event will take place on the track on December 3rd, 2021, so mark your calendars! Tickets are available to purchase at the student store, so make sure to get yours! It's sure to be an unforgettable event!

