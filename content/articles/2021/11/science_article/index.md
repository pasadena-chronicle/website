---
title: New Mineral Discovered Inside A Diamond
subtitle: What is Davemaoite?
writers:
- Brianna Gaughan 
date: 2021-11-15
topics:
- earth
- science
- environment
- discovery
thumbnail: davemaoite
caption: "Researchers discovered the mineral davemaoite inside a diamond that was formed in Earth's mantle. (Image credit: Shutterstock)"
---

Botswana is a large source of diamonds, exporting around 17 million carats in 2020. Among the many Botswana diamonds, a very special one was discovered. Within the diamond was a new mineral, one that was only ever predicted in a lab setting, never found in nature. Named davemaoite, this new rock can only have been produced in the Earth’s mantle - the layer of the Earth just outside the core. 

Scientists have predicted that davemaoite makes up around 5 to 7 percent of the mantle’s composition. This discovery is very significant, because minerals like davemaoite tend to change their crystal structure as they come up from the mantle and pressure decreases, making it very difficult to study mantle materials. The only other high-pressure mineral that has a confirmed existence, brigdmanite, was only found in a meteorite that hit Australia. Prior, it was only hypothesized to exist. 

Davemaoite tells us much more about the Earth than just mantle composition. With a molten liquid core and magma beneath its surface, Earth is very hot. Since it’s had 4.6 billion years to cool down, you’d think it would all be solid now. However, about a third of this heat is not just left over from the Earth’s past, but actually results from radioactive material. Inside the mineral structure of davemaomite are materials such as uranium and potassium, further confirming the idea that radioactive elements are a source of Earth’s heat. 
