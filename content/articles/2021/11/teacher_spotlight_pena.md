---
title: 'Teacher Spotlight: Mrs. Pena'
subtitle: Highlighting PHS's beloved Math-1 teacher
writers:
- Marley Thach
date: 2021-11-12
topics:
- teacher
---

Meet Mrs. Marian Pena, Class of 2024 Club advisor and devoted Math 1 teacher of I-106. You may have seen her selling concessions at our home football games, or on day one at freshman orientation helping new students find their lockers. Possibly you were taught a math process in a fun and interesting way or reminded of Wilson Middle School by her spirited presence on the PHS campus. Pena taught at Wilson until its closing in 2020. Now in her second year teaching high school mathematics here, Ms. Pena has wholeheartedly immersed herself in Bulldog culture. Her enthusiasm was clearly present in her voice when she described her involvement with the Class of 2024 Club, “I love school spirit and I was really involved with my high school so I knew when I started working here, I wanted to be involved with clubs and activities and that opportunity presented itself last year. We plan sophomore activities, fundraisers, campus deck, basically anything the sophomore class is doing, my club is involved.” 

Ms. Pena graduated from Texas A&M with a degree in business and Sam Houston State University, where she earned her Masters Degree in Education. “My mom was a teacher so it was a career that I always had interest. I had amazing teachers throughout my life that helped me grow, specifically my math teachers, I felt it was a passion of mine, to help kids.” In her ten years of teaching, she has earned a reputation with students who thrive under her kind and patient teaching style and engaging lessons. “She’s really, really, nice. I learned a lot when I had her, she really prepared me for the next level of math. I remember once when learning about ratios, she had us make waffles in her class with different ratios of the ingredients and then we ate them. Though I hadn’t had her in two years, she sent my middle school class a really sweet email wishing us luck on our first day of high school.” Madelyn Trang, her former Wilson student, recalled. “I like Ms. Pena because she is very patient and nice, she always asks if we need help and is always willing to help us with our work.” said freshman Jazlyn Yee. 

With the gender disparity present in math, computer science, and other STEM fields, female mathematicians can be underrepresented. When questioned about this she laughed as she recounted her unexpected path in becoming a math teacher. “It’s a fun story actually. I started out teaching computer science at Wilson. The principal at the time knew I liked to plan ahead of time so one day she told me ‘okay, you’ll be teaching math this year’ and I freaked out. But, I loved it and I will never teach anything else ever again.”

We are so very fortunate to have such a dedicated and engaged teacher in Ms. Pena and such a great representative of Bulldog culture.
