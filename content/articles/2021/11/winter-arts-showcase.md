---
title: Winter Arts Showcase Made Outdoors by COVID
subtitle: How PHS's Performers are Adapting to the Change
writers:
- Gwendolyn Lopez
date: 2021-11-22
topics:
- event
---

As November draws to a close, the PHS Performing Arts Department is busy with students preparing for December 9th’s Winter Arts Showcase. The Showcase is held annually in December, and it’s a night where PHS’s performers bring all their talent to the stage. This year, there will be performances from the orchestra, band, jazz band, drama, choir, and dance departments, as well as an art display from VADA and GCA.

However, due to pandemic circumstances, this year’s Showcase will be a little different than previous years’. Instead of being held in the auditorium, the entire event will be outdoors, and performers will be scattered throughout campus in “stations.” Although this is certainly not what the performing arts department was expecting, students and teachers are both working hard to make the upcoming Showcase a success.

The Winter Showcase will start at 5:30 P.M. The audience will enter through the C-gate, where they’ll be greeted by the jazz band. They will then go on to watch the orchestra and choir perform, followed by projections of VADA and GCA art. Next, the drama department will perform outside the auditorium. The band will play in the center quad, and the Showcase will finish off with a dance performance. Hot chocolate and food will be sold in stands.

I decided to get some student and teacher opinions on the Winter Arts Showcase—especially considering its unique circumstances. “It’s a lot more difficult this year preparing for everything,” said Mr. Hillig, the choir teacher at PHS. “We didn’t even know if we were going to be able to hold a live event.”

“Preparing for the showcase makes me nervous, but it’s because I care about the well-being of the orchestra,” added Zen Glick, a junior who is also the orchestra’s concert master. “I am sure we will do well!”

Turning toward the drama department, junior Ace Ellis told me they were “excited to participate in the Winter Showcase! It’s been very fun to work alongside my classmates on productions and learning more about acting and the logistics.”

The Pride of Pasadena Band is also hard at work. “Right now, the band is mostly wrapping up our fall marching season. We start working full time on Showcase music next week,” said Nalani Viray, a senior in the band. “I am really excited to see how it goes, especially considering the wandering layout.”

With all the time and dedication going into this show, there is no doubt that December 9th will be a night to remember. Be sure to spread the word about this event and mark your calendars—everyone is invited! After all, there’s no better opportunity than the Winter Arts Showcase to witness the accomplishments of PHS’ most talented performers and artists!
