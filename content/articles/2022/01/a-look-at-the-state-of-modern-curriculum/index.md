---
title: "A Look at the State of Modern Curriculum "
subtitle: ""
# A comma seperated list of capitalized names
writers:
  - "Leo Long "
date: 2022-01-17T01:01:43.560Z
# A comma seperated list of lowercase singular names
topics:
  - race
  - lgbtq+
  - academia
  - diversity
  - education
---
As the new year begins, many people are looking forward, trying to find ways to improve themselves and lead better lives. In a similar vein, it is important that we examine not only ourselves, but also those institutions which govern much of our lives, and education is no exception. What we learn in our schooling years is pertinent, not only to our future careers, but also how we go about understanding the world around us. It is thus vital to reexamine what it is we are learning, allowing us to understand how we are primed to interact with the world, identify potential issues, and then improve upon the status quo. PHS Junior Ace Ellis offered their insights on the subject, explaining that, while they have noticed a positive aspect of campus culture, in which certain instructors will sometimes fill in gaps that exist in standard curriculum, those gaps still need to be addressed. When it comes to the history of non-white groups, for instance, they put forth, “... there could be a lot of improvement on how we educate people about other races … it’s always so general… give me more of a relation ...you can see the clear bias, especially as a person of color”. They noted a lack of cultural diversity and how throughout high school, education seems to get increasingly European and U.S. centric. We discussed how this is especially true in the case of **A**dvanced **P**lacement courses, which tend to focus heavily on global history as specifically relates to the history of white cultures. Even classes with more of a diverse focus, they explained, typically only briefly cover non-white cultures, ignoring context that white countries tend to receive. 

In addition to racial issues, Ellis revealed what they felt to be an immense void in modern schooling: LGBTQ+ education. They noted that this was also an area in which history was failing, citing specifically the Stonewall riots, which they, as a member of the community, had not been exposed to at all throughout their high school career. Beyond history, however; they explained how more contemporary education on understanding non-heterosexual orientations and more gender fluid identities is severely lacking, and that such has fostered a popular mindset that is casually ignorant and thus inadvertently harmful to the LGBTQ+ community. Sociologically speaking, much of what Ellis discusses is reflected in what modern academia understands about education and its impact on students, for by leaving out context and information on minority groups, we as students thus develop our own ideas of what counts as “standard” and “normal”, resulting in less socially conscious behaviors as pertaining to said groups. What can be done? Well, Ellis believes the first step is simple, and states that the key to making education more inclusive, thereby developing more broadened minds, is to first just be more open to the idea of change, and accept that, maybe, it’s time to view the world in a different light.
