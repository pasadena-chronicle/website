---
title: The Truth Behind COVID's Spread
subtitle: As COVID numbers skyrocket in the past month
# A comma seperated list of capitalized names
writers:
  - Nolan Romero
date: 2022-01-18T01:08:22.337Z
thumbnail: cases_01132022.jpg
# A comma seperated list of lowercase singular names
topics:
  - campus
  - health
---
Ah winter break, a wonderful time to spend time with family and friends celebrating Christmas and New Year's Day. For some, this year was no different, but for most it was spend at home. Like many holiday **seasons** these past 2 years, **2021 holidays** **have** brought a spike in COVID **cases**. To many, this spike in cases has been a wake up call for anti-vaxxers and could change the way we go to school, **with a possible transition back to** online school.

Once winter break was over and we all returned to school, everything was normal at first. Going to class, hanging with friends, and **restlessly taking notes for** Mr. Michaelson's Human Geography. **After the first day, students have noticed** classes getting smaller, and friends disappearing all due to the one thing we know all too well, COVID. By the end of the week, a growing fear was building from fellow students wondering what will happen next. Like anyone else, I as well had my fears but also spurred a curiosity of how COVID had crept itself back into our daily lives.

**The omicron variant finds its origins in November of 2021**. Over the coming weeks**,** it would soon spread throughout the world, even causing some European countries to head back into lockdown. It eventually reached the U.S. on December 1, starting it's spread **in** the U.S.

We've all been fighting this pandemic for about 2 years now. For many people**,** having to deal with COVID has now become more of a burden that we have to deal with in our daily lives. People are staring to care less about their safety and protection of those around them. It's also due to the influence from people with platforms either spreading false information or believing there is no pandemic. Now as COVID is spreading faster then ever since the first outbreak, anti-vaxxers are beginning to get sick at greater rates, with some COVID victims realizing they were wrong and advising others to not make the same mistake they made.

If we deep dive into the real causes of COVID and its spread, it due to error-prone human nature. We can be far too stubborn to believe in the truth and face the reality of dire situations. We've made it this far and we can't simply give into this disease. Personally, I think we should stop worrying about the economics of remote learning/working while people are risking their lives and start caring about our health. 

But for now, may we stay safe during these hard times, and remain strong!
