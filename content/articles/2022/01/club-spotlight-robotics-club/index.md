---
title: "Club Spotlight: Robotics Club"
# A comma seperated list of capitalized names
writers:
  - Charis Graham
date: 2022-01-19T16:28:07.402Z
# A comma seperated list of lowercase singular names
topics:
  - club spotlight
  - robotics
  - stem
  - club
---
The PHS Robotics Club is a dedicated team of budding engineers, programmers, artists, and more. The club focuses on building robots to compete in the FIRST Tech Challenge (FTC), an international robotics competition for 7 to 12th graders. On the technical side of things, the Robotics Club assembles robots using Rev Robotics hardware and off-the-shelf materials; program robots in Java using a system called Android Studio; and uses tools such as the modeling software Fusion 360, 3D printing, and Tensorflow computer vision software. Members also work to organize fundraising and outreach events. They also develop graphic designs for t-shirts, flyers, and logos and engineering documentation to track the progress of the robot.

The club’s president, Jaimyn Drake, commented that, “People should join the Robotics Club to develop their skills.” Offering training in the aforementioned technical areas, the club also helps prepare for working in a cooperative environment. Drake further advertised that, “We make sure that all members develop the basic ‘soft skills’ that any employer looks for: public speaking, time management, basic writing chops, and organizational tech experience.”

One of the few clubs that managed to sustain full activity during last school year, the club has also consistently met Regional Championship qualifications for FTC. Unfortunately, not everything has been smooth sailing. The club used to house multiple competing teams. Struggles throughout the pandemic decreased that number to one. Furthermore, finances have been hit hard after two seasons without being able to fundraise due to the pandemic and mentor time is limited as the commitment can be overwhelming. Nevertheless, the club remains strong. Drake agreed that the mentors they do have (mainly Ms. Beck and Ms. Amaya, with help from Mr. Gardner, Dr. Chapman, and Ms. Orret) are amazing. The 20 to 21 members—especially vice president Brandon Nawagamuwa, secretary Christy Lam, and treasurer Charlie Weinberger—are dedicated and hardworking. In the words of Drake, “Our people are good enough to win tournaments: they just need the environment to actually make an honest effort.”

If you want to join Robotics Club, there are two typical meeting times: advisory in H208 (used for organizing work and communicating important information) and Tuesdays and Thursdays after school in E209 (used for robot building and more rigorous projects). You can support the club by showing interest or engaging in their activities. “The club is always looking to expand its communications network,” Drake explained, “so if you know any potential STEM groups we could collaborate with, have ideas about possible outreach opportunities, or would just like to visit us at work, that would be great.”
