---
title: PHS Junior Raises Over $3,500 Towards Cancer Research
subtitle: How a Cancer Survivor and His Fellow Orchestra Members Raised
  Thousands for a Good Cause
# A comma seperated list of capitalized names
writers:
  - Jasmine Sov
date: 2022-01-18T04:09:00.000Z
thumbnail: img_20220126_200102.jpg
# Only required if you have a Featured Image
caption: "From left to right: Zen Glick, Gwendolyn Lopez, Paris Zhang, and Dr.
  Anne Rardin."
# A comma seperated list of lowercase singular names
topics:
  - event
  - local
  - student
  - teacher
---
On November 17th, 2021, PHS junior Zen Glick, concertmaster of PHS’s Advanced Orchestra, took to the streets of Sierra Madre with his violin and one purpose in mind: to raise money for research towards neuroblastoma, a rare type of childhood cancer. Along with PHS orchestra teacher Dr. Anne Rardin (commonly known as Dr. R) and Advanced Orchestra students Gwendolyn Lopez and Paris Zhang, Zen performed in a string quartet at the Winter Festival in Kersting Court. 

The goal was $2,500. In the end, he successfully raised over $3,500 for Children’s Hospital of Los Angeles. 

A survivor of stage 4 neuroblastoma, Zen was diagnosed at age 2 and a half and was an inpatient at the hospital for a year. During our interview, Zen recalls going to the hospital at least a thousand times. “I didn’t go out of the hospital for a full year…I stayed until I was 3 and a half. I was an outpatient for about 2 years.” As a child in the hospital, he saw many other children enduring the same difficulties he had to go through.

I ask Zen how he got other members of the orchestra involved with his cause. “I’ve been meaning to do this for a while, so I asked a couple of good colleagues of mine [Gwendolyn and Paris] and I got Dr. R to play the cello as well.” 

A string quartet consists of two violinists, a violist, and a cellist. Violinists Paris and Dr. R undertook to play instruments they didn’t normally play, and all four went through hours of rehearsal in preparation for the event. Zen set up the fundraiser by talking to Robert Gjerde and Carol Canterbury of the Sierra Madre Chamber of Commerce.

On performance day, Zen, Gwendolyn, Paris, and Dr. R spent 3 hours without a break playing over 30 pieces of string quartet music—both holiday tunes and pieces from various musical eras. “We sat out at the base of the Christmas tree as a string quartet…People stayed! We took Paris’s viola case and we put it out because it was kinda big,” Dr. R recounts. “People threw money in.” They made $880 just from the in-person festival. Through the online fundraiser, Zen raised around $2,700.

All in all, the fundraiser was a major success, and it truly goes to show how dedicated the students and teachers of PHS can be when it comes to supporting a good cause. You can donate to the online fundraiser at <http://connect.chla.org/site/TR?px=2705776&fr_id=1090&pg=personal>. For more information about neuroblastoma, visit <https://www.mayoclinic.org/diseases-conditions/neuroblastoma/symptoms-causes/syc-20351017>.
