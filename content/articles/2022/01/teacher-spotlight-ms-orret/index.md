---
title: Teacher Spotlight- Ms. Orret
subtitle: ""
# A comma seperated list of capitalized names
writers:
  - Mallika Sheshadri
date: 2022-01-19T19:09:41.703Z
thumbnail: ms-orret-picture-1-.jpg
# Only required if you have a Featured Image
caption: Ms. Orret, App Academy teacher at PHS
---
I recently had the opportunity to interview one of my favorite teachers at PHS. Ms. Deborah Orret is one of the App Academy teachers and also develops a large part of the curriculum for the program.

“I’ve always liked Computer Science,” she says as we sit down. “When I was in 7th grade, I was really into Tumblr. But I didn’t have money to buy the themed backgrounds so I learned how to code them myself using HTML and CSS.” 

Ms. Orret’s interest in computer science continued to grow throughout high school and she majored in Cognitive Science with a specialization in Computer Science. “I graduated from UCLA in 2020.” she continues, “I didn’t think I would end up teaching straight out of college, I just fell into it.” 

Aside from being one of the lead teachers in App Academy, Ms. Orret is the supervisor for over five clubs, and also created the 28% - A Newsletter for Women in STEM.  

“When I came to PHS, I was immediately aware of the gender gap in App Academy.” she explains, “ I was always one of the few females in my Computer Science classes. Only 28% percent of researchers, scientists, and engineers are women.”

This gender gap is immediately noticeable, even at PHS. There are only 4 freshmen girls in App Academy right now, while there are more that thirty boys.

“Why do you think the Computer Science field is so male dominated?” I ask, curious to hear her take on the ever-present issue.

“I think it’s mainly due to how it’s marketed,” she begins. “Video games, which are primarily targeted toward boys, are a quick pipeline toward coding and computer science. What video game do you know that doesn’t have a male as the main character?”

A large part of this disparity comes from the societal stereotype that only men should be educated and make money. Even though the world is moving away from this ideology, it’s still present in the world of STEM. Commonly, the world of Computer Science is affiliated with the concept of building massive, multi-billion companies like Amazon and Facebook.

“People like Mark Zuckerberg and Jeff Bezos, who are in my opinion, slimy men, are currently the face of the field, which makes it harder to spark an interest in girls and women.” Ms. Orret says emphatically.

“I think that cultivating a diversity of interest and combining coding with other sciences in App Academy is one of the biggest ways to draw in more girls. There is just so much that women can offer to Computer Science, but it’s hard to be the only woman in a room of men. That’s why I created the 28%. I wanted to give the Women in Stem at PHS an opportunity to speak. It doesn’t matter who reads it, but at least I can give them a voice.”