---
title: Will we stay in school?
subtitle: Alarming COVID-19 numbers leave students wondering about a return to
  online learning
# A comma seperated list of capitalized names
writers:
  - Polly McConnell
date: 2022-01-16T18:54:31.211Z
thumbnail: image0.jpeg
# Only required if you have a Featured Image
caption: "All-time COVID-19 cases in California (Source: NY Times)"
# A comma seperated list of lowercase singular names
topics:
  - health
  - student
  - campus
---
Winter break brought many new things - cold weather, relieved stress, holiday spirit… and of course, a tremendous **COVID-19** spike. A rocketing holiday surge of the Omicron variant left many in shock and panic, especially as similarities began to emerge between our current situation and that of March 2020. Across the globe, countries have made moves to recede back into semi-lockdown. Case numbers are rising by the thousands each day, despite widespread vaccinations and booster shots. 

Yet, perhaps most shocking in this strange time is the notion to return back to school. Families throughout the U.S. are sending their children into the classrooms for a second semester, and many schools - like PHS - are proceeding as usual. This decision is controversial given the skyrocketing numbers of the holiday spike, which continue to rise. 

Last year, students’ lives were uprooted by the pandemic. Many watched their grades, mental health, and social life crumble as they were forced into online schooling. Now, case numbers are at an all-time high, everyone knows a friend who has tested positive, harsher protocols are being enforced… So how is it possible that, amongst all of these closures, school remains open? It’s only a matter of time before we return to online learning. Right?

Wrong. Believe it or not, staff at PHS confirm with confidence that we will remain open. 

One of the main sources of income for a federally funded school (such as ours) is attendance. The school receives funding every day that you show up… hence, if PHS closes and becomes virtual, nobody’s coming onto campus grounds, and PHS makes $0 as opposed to $1,224 per student (Education Data Initiative). Multiply that by our 1,700 students, and PHS would lose over a million dollars in a year. It should be noted that there is currently a bill in the California legislature to base federal funding on enrollment rather than attendance - but should this be passed, it wouldn’t kick into effect until 2023. 

Safety-wise, while Omicron might be a contagious variant, low death rates prove that it’s far less lethal than delta - especially for the vaccinated. And Pasadena, thankfully, has an almost 90% vaccination rate (Pasadena Public Health Dept.) 

So, yes, school will remain open. That’s not to say, however, that no precautions should be taken. Many schools and establishments are requiring frequent testing, boosters, and stricter protocols. 

It might feel weird to move six feet away from your friends while you eat lunch, or sanitize after every class, but the quicker we can overcome this surge, the better. Stay safe, and let’s get through this second semester!
