---
title: 7 Ways To Honor Black History Month
# A comma seperated list of capitalized names
writers:
  - Marley Thach
date: 2022-02-14T18:51:09.410Z
---
Since 1976, America has dedicated the month of February to honoring the contributions of African Americans to U.S history. We can commemorate this month in numerous ways, from supporting Black owned businesses to visiting local museums, and taking the time to remember the people of our history, here are seven ways to celebrate black history, this month or any other.

1. Visit the California African American Museum

Founded in 1977 and located in Exposition Park, steps away from USC campus, the California African American Museum’s mission exhibits a vast array of African American art, culture and history  with an emphasis on California and the western United States. 

2. Visit LACMA’s Black Portraits Exhibit

Los Angles County Museum of Art is currently showing the Black American Portraits gallery. This exhibit remembers two centuries of Black American Art, form the 1800s to present days consisting of 140 pieces portraying love, community, and exuberance in an expression of black strength rather than suffering. This experience is available now through April 17, 2022.

3. Visit Regeneration: Black Cinema 1898–1971

Looking for an out of the ordinary selection of black excellence? Visit  The Academy Award Museum’s exhibit “Regeneration: Black Cinema 1898–1971”, a new exhibit that strives to “redefine US film history by elevating this underrepresented aspect of artistic production and presenting a more inclusive story”.  The exhibit, which received the 2018 Sotheby’s Prize, highlights Black participation in American film making and was recognized for curatorial excellence. 

4. Listen to the New York Times Podcast “1619”

 Providing information to those without heavy knowledge of the subject as well as information to those who have already obtained some, "1619" covers the effects and history of American slavery. 

5. Educate yourself on Historical Black Figures

The education of African American history month usually consists of coverage on figures such as Martin Luther King Jr., Harriet Tubman, and Rosa Parks, all of whom fundamental in our history, but what about the other figures? Kariamu Welsh Asante, dance trailblazer, or Chokwe Lumumba, civil rights attorney, people whom the textbooks don’t speech of. To learn more of our history visit blackpast.org, a website presenting a variety of African American icons.

6. Support Black Owned Businesses

During the COVID-19 pandemic, Black business ownership declined more than 40%, a disproportionate and discriminating number. This month especially it’s important to support the black community. Unsure of how to find businesses? The website miiriya.com showcases thousands of black owned products, or simply use#blackowned to explore more on social media platforms.

7. Donate to Anti-Racism Charities

With the Black Lives Matter movement in mind now is as good a time as ever to donate to charities to help  the Black community find justice. Charities to consider donation to include the [](http://blackorganizingproject.org/)Black Organization Project, Black Youth Program, or Loveland Therapy Fund to start.