---
title: Art on the Ballot
subtitle: A new initiative for arts education in California schools
# A comma seperated list of capitalized names
writers:
  - Polly McConnell
date: 2022-02-19T22:25:07.891Z
# A comma seperated list of lowercase singular names
topics:
  - student
  - art
  - cirriculum
  - campus
---
Throughout the history of the public education system, art has been consistently underfunded in K-12 schools. Even today, most students who are remotely interested in art as a career opt to attend a specialized arts high school that’s an hour away rather than the public school down the road. 

For public schools, art is almost always set on the backburner; treated as an afterthought rather than a fundamental tenet of education. The neglect is evident right here in California, where only one in five schools have a full-time credential arts or music teacher, and when it’s time to make budget cuts, art programs are often the first to go.

Recently, a new initiative is emerging to combat this issue. The ballot is called Californians for Arts and Music Education in Public Schools, and it ensures $800 million - $1 billion annually to *every* K-12 public school in California for the purpose of arts education. It guarantees growth of these programs by at least 50%, and implements transparency measures to verify that schools allocate the funds as intended. 

70% of the money is distributed according to enrollment, and the remaining 30% is added to schools that serve low-income and minority students, which reflects the ballot’s focus on creating a more equitable access to arts education. Most impressively, this would all be achieved without raising taxes.

The initiative is supported by a broad range of organizations and individuals, including Fender Music, NBC, and even Dr. Dre. 

How would this measure take effect at PHS? First off, it would no doubt expand our CAMAD (Art Academy) program. We would see funds to hire additional staff, new art supplies, and opportunities to partner with art and community organizations. Additionally, our performing arts - band, Orchesis, dance class, drama, musical theater - would receive the same benefits. This means newer instruments, materials, facilities, props; bigger assemblies and pep rallies; large-scale production musicals. The Arts and Music in Schools Initiative would even improve our App Academy program (as coding is considered a form of art). 

To qualify for the official November 2022 ballot, the measure needs 623,000 signatures by May 1st, 2022. If you’re passionate about funding for PHS’ arts programs, or perhaps California public schools on a larger scale, there are various ways that you can help make this ballot a reality. If you’re of voting age, you can help collect signatures to legitimize the initiative, and if you’re not, you can spread the word to your parents and teachers. 

Californians for Arts and Music Education in Public Schools is a shining opportunity to improve the mental health, equity, and spirit on campuses across the state; it is one crucial step in the larger movement to reform the values and practices of education as a whole.
