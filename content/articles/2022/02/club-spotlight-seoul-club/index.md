---
title: "Club Spotlight: Seoul Club"
# A comma seperated list of capitalized names
writers:
  - Gwendolyn Lopez
date: 2022-02-19T02:51:37.913Z
thumbnail: img_2869.jpg
# Only required if you have a Featured Image
caption: From left to right, the club president Yeonho Jeong and the vice
  president Grace Yang. Photo by Juha Kim.
# A comma seperated list of lowercase singular names
topics:
  - club
  - student
---
Since last fall, you’ve probably seen the pastel-colored flyers scattered around campus, promoting the return of the Seoul Club. With a minimal, elegant design—including pictures of K-pop groups and the popular K-drama, Squid Game—the flyers have certainly piqued the interest of many students, including myself. So, in order to uncover more about the club, I set off to interview its new president.

The Seoul Club had been around since 2018, but a lack of membership during the pandemic caused it to disband. As a result, when students came back to campus in the fall of 2021, hardly anyone knew that the Seoul Club had even existed. That is, until sophomore Yeonho Jeong resurrected the club in late October.

She had lots of support along the way. “There’s three of us,” Yeonho told me when I asked her about it. “The vice president, Grace Yang, and our treasurer, Claire Brandon.” What started off with just three people has now grown into a community of about ten members—and this is only the beginning.

“What inspired you to start the club?” I asked.  

“I saw that Korean culture was becoming more known recently,” Yeonho replied, “and I wanted to do something that could share it to everyone. This way, we could all learn more about it.”

“And how do you accomplish this during club meetings?”

“It depends. Sometimes we have movie days where we’ll watch Korean movies, and other times we’ll play traditional Korean games. Every week it’s something different.” 

For instance, one of the aforementioned games that the Seoul Club has played is Yut Nori (윷놀이), or “Yut Game.” It’s a type of board game where players use “Yut sticks” instead of dice to accumulate as many points as possible. The club has also watched the popular Korean movie Train to Busan (부산행): an action-packed zombie apocalypse film.

As the interview drew to a close, I pondered over what Yeonho had mentioned: the popularity of Korean culture. It’s definitely obvious how Americans, especially teenagers, have been impacted by Korean influences—whether it’s through music, TV, or fashion. Nowadays, social media seems to be teeming with all sorts of Korean “trends”—most of which Americans tend to generalize as simply “Asian.” Instead of bending to the will of these trends, I encourage you to do your own research on them. This way, we can all educate ourselves and celebrate the unique beauty of Korean culture!

With that in mind, be sure to check out the Seoul Club! They meet every Wednesday during lunch in room E210.