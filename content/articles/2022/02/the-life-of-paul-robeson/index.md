---
title: The Life of Paul Robeson
# A comma seperated list of capitalized names
writers:
  - Leo Long
date: 2022-02-14T00:31:39.256Z
thumbnail: paul-robeson.jpg
# A comma seperated list of lowercase singular names
topics:
  - politics
  - race
  - music
---
It is a common adage in sociological circles just how overlooked are the contributions of African Americans to popular American culture, especially in music. From the slave spirituals that would go on to inspire blues and gospel to the Jazz Age and Harlem Renaissance of Louis Armstrong and Billie Holiday, it seems that any major movement in American music is strongly rooted in the Black community. And so in honor of Black History Month, it seems appropriate to analyze a prominent figure whose work inspired and uplifted millions, who challenged the status quo of the world he lived in, and who dedicated himself to the plight of freedom: Paul Robeson. 

To understand Robeson one must first look to his youth. Robeson was born in 1898 in New Jersey to minister William Drew Robeson and Quaker Maria Louisa, the former of which was actually a former slave who had freed himself by fleeing the plantation as a teenager. In terms of his education, Robeson attended various institutions, including Rutgers University, Columbia Law, and, pertinent to his later work, the School of Oriental and African Studies where he demonstrated a passion for African history. Despite having a law degree, Robeson actually ended up abandoning the law as a profession, in large part due to issues Robeson had with racism that permeated much of the field. 

Eventually, Robeson began to find a career on the stage, having gained initial fame for his performances in *All God’s Chillun* Got Wings and later *The Emperor Jones*. This initial foray into stage life would only grow and it would be his role in Show Boat where he would give his landmark rendition of *Ol’ Man River*, demarcated by his distinctly baritone voice, elevating him to mainstream appeal. 

What truly transformed Robeson’s life, however, would be the Spanish Civil War, during which he expressed sympathies for struggles against fascism, and came to be intensely involved in political activism. Robeson used his platform and influence to advocate for a variety of causes, having openly denounced the U.S. for systemic racism and advocated for staunchly left economic reforms. He would prove to be so outspoken that during the Cold War, Robeson would actually be blacklisted by the U.S. for his anti-capitalist views and sympathies for the Communist principles of the Soviets. 

Robeson would continue to voice support for causes he firmly believed in, and infuse those beliefs into his work, up and until the very end when he died at the ripe age of 77 in Philadelphia. 

This has been a very brief look at just one of the many Black artists who made a name for themselves not just as musicians but also as political figures, and this is by no means the whole story on Black musicians in America, or even on Robeson himself. However, what we mustn't forget is just how significant and innately intertwined Black history is to American history, and the power of art in critiquing the world in which we live.