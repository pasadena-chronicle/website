---
title: "Announcement: ASB Elections "
subtitle: ASB Elections are right around the corner!
# A comma seperated list of capitalized names
writers:
  - Charis Graham
date: 2022-03-11T20:05:07.164Z
# A comma seperated list of lowercase singular names
topics:
  - asb
  - elections
  - pasadena high school
---
The 2022 Spring ASB Elections are now open for applicants to apply! Applications are available in D101. There are several positions available, including President (head of the ASB cabinet), Vice-President (understudy for the President), Secretary (organizer and minute recorder for ASB), and Treasurer (overseer of ASB related income and expenditures). There are also various Commissioners of everything from elections to social affairs, a Student Board Representative (representative to school board meetings at the Board of Education), and representatives for different class years. More information regarding the positions can be found on Canvas or requested in D101. 

Applications are due by Thursday, March 24, 2022. Candidates should be aware that they must have a minimum 2.0 GPA and no F’s or U’s. Candidates are required to attend the Election Meeting at lunch on Tuesday, April 12th. Those who are eligible will be given instructions on how to campaign.

The elections will be held online on Wednesday, April 20th and Thursday, April 21st. Interviews for appointed positions will be held on April 27th and 28th.
