---
title: Covid mask mandates dropped
subtitle: California students can now ditch mask outdoors
# A comma seperated list of capitalized names
writers:
  - Nolan Romero
date: 2022-03-14T21:31:29.412Z
# A comma seperated list of lowercase singular names
topics:
  - health
  - campus
  - students
---
 Starting today in California, Mask mandates outdoors will be lifted. This has been part of a movement to lower Covid or put an end to mask mandates in the west and soon all of the U.S. California is not the only state putting an end to their mask mandates; Oregon and Washington will no longer require masks outdoors as well. Within the U.S, we've seen a slow decline in Covid cases since the huge spike in mid. January. Since then, more and more states have decided to drop the mask mandate for the outdoors or altogether.

   From a state masking policies map from September of last year, 17 out of the 50 states held state mandates, with even a few states such as Florida banning the possibility of mask mandates altogether. This alone is a bit concerning, as the other 33 states have no enforcement or a total ban of mask mandates. Though, the one problem with this data is it's about 6 months outdated. But we can turn to an article written by NBC News from February of this year which includes another masking policy map. This newer map reveals how only now 8 states have any sense of a mask mandate. Using the data received from both maps, we see common patterns in them both, one obviously being the decrease in the already small number of states with mask mandates. Another pattern is that many of the states with no or total ban of mask mandates are in the midwest or in the center of the U.S., which for the most part are Republicans, likely being the cause for no action within those states.

Coming back around to the main point, we see how the U.S mask mandates are slowly fading away. A state I've yet to take about is Hawaii, which until the end of this month will be the one state which will continue to hold its mask mandates. The mask mandates will remain in place until March 25, when they will come to an end. On Monday of next week, we will see some schools beginning to allow students to attend maskless altogether. Through the weekend and the coming weeks throughout Pasadena and beyond, we will see more and more people without masks. To keep yourself safe, you should remember to keep your mask on and keep 6 ft or more away from others. Although right now where mandates are dropping like flies, remember to keep yourself safe and protect yourself.
