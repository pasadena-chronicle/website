---
title: APP Academy Teacher Reflects on Equity and Curriculum in PHS's Computer
  Science Pathway
subtitle: Ms. Orret is paving the way for a learning environment that is
  inclusive for all
# A comma seperated list of capitalized names
writers:
  - Morgan Gaskell
date: 2022-10-03T15:00:00.000Z
thumbnail: 72c3843b-53e8-4166-bf49-aeebb8b95afa-3-.jpeg
# A comma seperated list of lowercase singular names
topics:
  - club
  - teacher
  - district
---
This is only Ms. Deborah Orret's third year teaching APP Academy at Pasadena High School, and she has already made a huge impact on the learning environment and on the students. The ways Orret fuses fun and learning in the classroom makes for a stellar learning environment where students feel engaged and heard.

Orret majored in cognitive science at UCLA, with a focus in Computer Science. Right after graduating in 2020, she took a job at PHS as the newest addition to the APP Academy team. She also took on the role as one of two curriculum developers for the pathway. Orret has been a popular teacher since day one, where she gave enthusiastic lectures and assigned fun projects over Cisco WebEx during the 2020-21 school year.

Orret's passion for helping and connecting with students is reflected in the number of clubs she advises. First and foremost, she helped create the 28% Newsletter after one of her students voiced her concerns about being the black sheep in a class of all boys who were well versed in computer science. The 28% is a monthly newsletter written by girls at PHS who have a passion for getting other women interested in STEM. The newsletter features scholarship opportunities, interviews, and biographies on influential women in STEM. Orret's reason for collaborating with her students to create the 28% is inspiring. She says in an interview conducted by the author that she too has felt like a minority in the sciences, for instance in her college classes. She shares how she wanted to find a way to create active change in the classroom, to create a space for girls to take up space and be heard. 

Additionally, Orret is the advisor for the Design Lab and Gaming Lab, two afterschool clubs that she created during the 2020-21 school year that focus on animation, 3D modeling, video game design, and robotics. Last year, Orret was also the advisor for the Fashion Club and Baking Club. It seemed every day, Orret's classroom is bustling with curious students. 

In the classroom, Orret notices the apparent gender gap in students interested in computer science and programming. She shares over the interview how it is apparent that girls are the minority in APP Academy, with perhaps only 20% of freshman classes being made up of girls. It has been estimated that women only make up 28% of the STEM workforce (the namesake of the 28% newsletter). Within this percentage, women are most underrepresented in technology and engineering. PHS's APP Academy is no expectation to this statistic. 

Orret works very hard to ensure the curriculum she designs is as meaningful and intentional as possible so as to be more inclusive to a wider audience. Her techniques are very much rooted in equity and she communicates with students to hear their feedback on her teaching. She believes that curriculum shouldn't be a stagnant thing, but a flexible, ever-changing process of teaching students in ways that work for them. Orret strives to create a curriculum and classroom that is inclusive of all minority groups. She hopes doing so will get everyone thinking that computer science is something they can do.
