---
title: "Club Spotlight: Anime Club"
# A comma seperated list of capitalized names
writers:
  - Hudson Zortman
date: 2022-10-03T15:00:00.000Z
# Only required if you have a Featured Image
caption: ""
# A comma seperated list of lowercase singular names
topics:
  - club
  - student
---
Whether you are a freshman or a senior, I am sure you have probably seen flyers scattered around campus of iconic anime characters like L and Gojo, with a pastel-colored background and the words "Anime Club" above them. As a student who has been interested in learning more about what the Anime Club is, I set off to interview the club president, Ruby Lee, for all the details. 

“I've been a long time fan of anime and manga, so when I heard there was a club for it, my friends and I joined immediately. It's a nice space where you can talk about your interests, while also meeting new people and possibly discovering a new show or game. There is also free candy and snacks, because everyone loves free candy!” The club is casual and is always looking for new members to join.

I spent a week in the Anime Club, and it is usually pretty chill. We vote on an anime to watch and enjoy as a group. Ruby also told me that they are trying to come up with new activities.

The club is still pretty small so they're always looking for new members. If you're a veteran or newbie to anime, everyone is welcome. If you are new to anime, Ruby says that “Death Note" is a good anime to start with because it's interesting and enjoyable even if you are not familiar. If you are interested in joining the Anime Club, meetings are  every Tuesday and Thursday after school in room G208.
