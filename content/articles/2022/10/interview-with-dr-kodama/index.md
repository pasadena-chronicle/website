---
title: Interview With Dr. Kodama
subtitle: Who is he, and what are his goals for PHS?
# A comma seperated list of capitalized names
writers:
  - Gwendolyn Lopez
date: 2022-10-03T16:00:00.000Z
thumbnail: img_0745-1-.jpg
# A comma seperated list of lowercase singular names
topics:
  - teacher
  - district
  - principal
---
As the school year begins, PHS gives a warm welcome to its new principal, Mathew Kodama. You’ve certainly heard his name and read his emails, or maybe even seen him around on campus. But what exactly does a new principal mean for the students of PHS? What can we expect from our new administration as we go into this new school year, and the many future ones to come?

First, our new principal isn’t as “new” as some might think. Dr. Kodama has been a part of PUSD since his high school years, when he graduated Blair High School in 1989. “I’ve had deep family roots in the Pasadena area,” he told the Chronicle in an interview. 

Dr. Kodama initially had no intention of going to college. Instead, he started working as an instructional assistant at PUSD. Only then did he begin to consider a career in education. “People I would work with kept coming up to me and saying, ‘Hey, you could be a teacher,’” Dr. Kodama said. However, he still held reservations against college for a while—there was the strong fear of taking that next step, of exposing oneself to a new environment that family members couldn’t relate to. But ultimately, the support of public school educators won him over.

“I started out at PCC, taking as many units as I could, and working full-time.” From then on, Dr. Kodama completed a bachelor’s degree in Human Development from Azusa Pacific University as a first generation graduate. He then went on to pursue his masters, and got his doctorate from USC. 

Around ten years ago, Dr. Kodama came to PHS as an assistant principal. He was in charge of working with counselors, overseeing the AP program and master schedule, and working with special education. “At first, I was incredibly intimidated because this is the largest school in Pasadena Unified,” he told us. “But I fell in love with Pasadena High School, with working with all the students.”  

Now, after seven years of working in another district, Dr. Kodama is back at PHS. “I knew that it was time to return,” he said, “and to bring back some of the skills I had learned in other positions... to support our students thriving in every aspect of their life on campus.”

This year, Dr. Kodama has three major goals for PHS: communication, campus beautification, and safety. “My first priority is to listen and connect,” he said. “To listen and to understand teachers, staff members, and students.”

As for campus beautification, the district will be working through Measure O, a bond approved by community voters for PHS’s facility improvements. Starting from October, Dr. Kodama will invite both parents and students, as well as other members of our community, to discuss how to best use this resource. “\[We’ll] also look at that beautification scale on campus, so that our exterior reflects the excellence that takes place in our classrooms—and the learning that happens here—every day.” 

And of course, Dr. Kodama wants to make sure that all students and staff members feel safe on campus. "We need to look at all of those safety protocols to make sure that you are all safe when you are in school," he said.

In terms of funding, Dr. Kodama hopes to increase support staff and continue the many programs at PHS—not just APP Academy, CAMAD, and LPS, but also other programs, such as athletics and the performing arts. “It’s looking at those exciting programs and opportunities and seeing how we can enrich those by using our resources,” Dr. Kodama said. In addition, he wants to increase the number of AP course offerings available at PHS. “Everything that we’re doing in the allocation of resources should be toward building greater and greater levels of opportunities for every student on campus.”

With all of these goals in mind, Dr. Kodama will try his hardest to make PHS the best that it can be. “I really want to connect with every student on campus,” he said. “So I can understand where your hopes, dreams, and goals lie.” If you ever see him around, don’t be afraid to say hello!
