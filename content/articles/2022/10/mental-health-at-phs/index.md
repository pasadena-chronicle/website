---
title: Mental Health at PHS
subtitle: How has PHS been feeling in the past year?
# A comma seperated list of capitalized names
writers:
  - Jasmine Sov
date: 2022-10-03T15:00:00.000Z
thumbnail: f0be9fb5-db84-4eb9-94d3-6209c939268f-1-.jpg
# A comma seperated list of lowercase singular names
topics:
  - student
---
It’s no secret that the past few years have taken a big toll on PHS’s students. Over the course of the pandemic, I witnessed a drastic shift in my friends’ personalities—many of them became exhausted, depressed, and angry at the world. This made me wonder: if this was how they felt nearly every day, then how must the rest of the students at PHS be feeling during this time?

With this in mind, I conducted a survey on the mental health of the PHS student population from April 14 to April 19 of 2022 and received 368 responses—an estimated 16% of the school’s student population.

**Questions on the survey**

Questions on the survey included the following:

* A scale of 1-10 asking students how content they felt within the last month, where 1 was “extremely dissatisfied” and 10 was “perfectly content”.
* A scale of 1-10 asking students how stressed they felt within the last month, where 1 was “extremely stressed” and 10 was “perfectly calm and collected”.
* A checkbox question directing students to pick up to 3 characteristics that afflicted them out of 5, the 5 characteristics being stress, anger, sadness, anxiety, and inattentiveness.
* A free-response question asking students why they thought they felt the way they did.

**Findings**

* Contentedness:
  * Students at PHS are somewhat content with their lives. The majority of students (21.3%, or 76 students) rated themselves a 7 on the contentedness scale. The second largest majority (18.8%, or 67 students) rated themselves a 5. 
* Stress:
  * The stress levels of students at PHS show much more alarming statistics. 16.5%, or 59 students, rated themselves a 3 on the stress scale. The majority of students rated themselves a 5 or below on the stress scale (55.7%, or 199 students)
* Characteristics that afflict students:
  * Stress is the most prevalent characteristic that afflicts PHS students—85.7% of all respondents, or 306 students, selected stress on the checkbox question. The second most prevalent characteristic is anxiety, with 59.7% of all respondents (213 students) selecting anxiety on the checkbox question.
* Why students feel the way they do:
  * School was a large factor in many students’ responses; around 44.8% of all respondents (160 students) mentioned school in their explanations as to why they felt the way they did.
  * More specifically, many students cited missing/piling up assignments and being frustrated with the amount of work given by teachers. Others talked about worrying about their grades, being distracted from work, and having to become accustomed to completing assignments post-pandemic.
* Possible flaws in the survey:
  * Restriction of students to 3 characteristics out of 5; not providing an “Other” or “N/A” option. Some students stated that they wished to select more and others stated that they felt none of these.

In the free response question, some students expressed concerns about extracurriculars and college prep-related activities as well:

“Stressed with recent tests and quizzes and also having to get prepared for AP tests. Also getting ready for a busy summer full of writing my college essay, taking PCC classes over the summer, visiting and determining which colleges I want to apply to, having an internship with my academy, and coming to the realization that high school is almost over and college is around the corner.”

“I think I feel this way because I have taken on a lot of new commitments and I don't have the time to complete them. This causes more stress on me. This makes my anxiety skyrocket as well. I've also had some problems with my friends in my life so that has made me sad.”

**Proposals to improve the mental health of students at PHS:**

* Expand PHS’s mental health program beyond just the Wellbeing Center—advertise the Wellbeing Center more around campus, and encourage students to take advantage of the school's therapist.
* Work to destigmatize mental health struggles and ask for help; despite many initiatives and programs employed in K-12 schools and colleges, many students do not feel comfortable asking for help. Rethinking mental health needs in the same vein as physical health will help to encourage seeking help. 
* Teach students to recognize various forms of abuse and to report it to a trusted adult.
* Advertise teen mental health resources provided by the City of Pasadena. There are many resources provided by the City that are underutilized, simply because no one knows about them.
