---
title: New Clubs
subtitle: Club Rush 2022
# A comma seperated list of capitalized names
writers:
  - Mallika Sheshadri
date: 2022-10-03T15:00:00.000Z
# A comma seperated list of lowercase singular names
topics:
  - club
  - student
  - event
---
At this year’s Club Rush, PHS students saw a surplus of new clubs and organizations. With over 50 clubs now on campus, there’s definitely a space for everyone! Here are some of the newest clubs to come to our campus!

Baking Club isn’t a new club, but it is branching out this year with its new Culinary Club. In addition to weekly potlucks on Mondays, president Shane Vandevelde plans to implement cooking days on Thursdays at lunch, where the Culinary Club can cook together in the club’s new cooking room. 

Jasmine Gluck is the president of the new Book Club. The club will be discussing questions about different books and hopes to volunteer at book drives! The club will be meeting on Fridays in E208.

A returning club you may not have known about is the FIDM Fashion Club. Run by Sharon Saenz-Godinez, the club is for people who love all things fashion. The club hopes to design mascots and products for other clubs, especially the shirts for Robotics Club this year.

DIY Club is a great way to use your creative skills for a good cause. This club hopes to make money selling handmade jewelry so the money can go towards donating toys to a toy drive. There’s no meeting day yet, but you can contact the club’s president Lindsey Nuno for more information.

The Mental Health Awareness Club is another new addition to the campus. Run by president Gianna Carmente, the club will be meeting Wednesdays at lunch. The club hopes to destigmatize the topic of mental health, do therapeutic activities, and create a relaxed space for people who are struggling. They also hope to have guest speakers and fundraise money to donate to organizations that help people who are going through hard times.

The Sexuality and Gender Alliance (SAGA) has returned to PHS after a one year hiatus! The club aims to create a safe space for LGBTQ+ folks and allies through social and support groups and will also do fundraising for local queer organizations. The club meets every Tuesday at lunch in C106. You can contact the club’s president Morgan Gaskell for more information.

The Creative Writing Club is a great outlet for writers of all sorts! The club gives out writing prompts for fiction, nonfiction, poetry, creative genres, and screenwriting. Creative Writing Club aims to create a space for fellow writers to give constructive criticism to one another. Run by Jasmine Sov and Gwendolyn Lopez, the club meets on Thursdays after school in room I207.

Game Club is the place to sharpen your chess skills, master the art of Dungeons and Dragons or just meet new people! Run by president Andrew Aldana, the club has a variety of board games to play every Wednesday at lunch in Mr. Archuletta’s classroom, P105.

Business Club, run by Kaya Lorentzen, is a chance to be an entrepreneur and meet like-minded people. Club members will be working together to create, pitch, and sell a product every Friday at lunch in G207.

The Anime Club is another new addition to campus “We watch anime. It’s pretty calm and chill and we just joke around,” says club member Reagan Robertson. The club, run by president Ruby Lee, meets on Tuesdays and Thursdays in G212.

The Hiking Club is for all hikers and nature lovers. On their twice-monthly hikes, the club makes an effort to leave the trail better than they found it, taking with them reusable cotton bags to pick up trash. They meet on the first and third Mondays of every month in H208 and hike on the Saturdays after each meeting. 

The Future Voters Club encourages those who are old enough to use their voices and vote. The club will be hosting a voting registry drive prior to the 2022 midterm elections. You can contact president Bryce Wienberger for more information.

The Future Medical Professionals Club, run by Shirley Woodard, works to expose students who are interested in the medical field to opportunities available to them. Activities will include toy drives, meeting with doctors and nurses, and volunteering at local hospitals. They meet every Monday at lunch in room H106.

The GLI Club is a woman empowerment organization run by Girls Learning International. They will be doing lots of fundraisers to support women in need and hope to raise money for better menstrual products in bathrooms. You can contact president Michelle Cortez-Peralta for more information.
