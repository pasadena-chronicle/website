---
title: Suicide Awareness Month
subtitle: There is always help for you! You are not alone.
# A comma seperated list of capitalized names
writers:
  - Rubi Sherman
date: 2022-10-03T15:00:00.000Z
# A comma seperated list of lowercase singular names
topics:
  - student
---
September is Suicide Prevention Awareness Month. For many people, suicide is a tough topic to discuss. Even though suicide is something most people prefer not to talk about, bringing awareness to the issue is incredibly important. Talking about it allows individuals who are dealing with thoughts of suicide to feel like they are not alone. Talking about suicide can also help change our society’s perception of the topic. 

In high school, there is so much pressure and stress students have to deal with. On top of that, our bodies are changing, and we are experiencing many emotions at once. It’s tough to deal with it all, but we can all work together to help each other through it. For example, providing a safe space and support for someone or just telling your friend or family member you appreciate them can really help how a person feels and thinks. You never know what someone might be dealing with inside even if they present themselves as happy and bright. As a teenager, words can be very hurtful. You may think something is funny or “just a joke”, but people can take the things you say in the wrong way. That's why thinking before you speak is so important. 

Learning the warning signs of suicide is a huge part of preventing a crisis. Some warning signs of suicide include a change in personality or appearance, hopelessness, severe sadness, extreme mood swings, talking about wanting to die, or thoughts of self-harm. Suicide is preventable, and there are so many resources out there to help. 

If you or someone you know is struggling or in crisis, help is available.

 Call or text 988 or chat at 988lifeline.org. Veterans, press 1 when calling.

Or text TALK to 741-741 to text with a trained crisis counselor from the Crisis Text Line for free, 24/7

To receive help in person you may also talk to your:

* Primary care provider
* Local psychiatric hospital
* Local walk-in clinic
* Local emergency department
* Local urgent care center
