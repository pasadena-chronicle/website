---
title: "Teacher Spotlight: Mr. He"
# A comma seperated list of capitalized names
writers:
  - Jessica Lewis
date: 2022-10-03T15:00:00.000Z
thumbnail: 6-1-.jpg
# A comma seperated list of lowercase singular names
topics:
  - teacher
---
Mr. He, a Mandarin teacher at PHS, is a very accomplished man. Not only can he teach multiple languages, but he can also incorporate every student's needs. He is a graduate of Cal State San Bernardino, with a master’s degree in education, and now teaches Mandarin 1,2, and 3, along with AP Chinese. Although he is still a little new to teaching with only two years in the field, he has managed to make a great impact on the school. Mr. He enjoys being a teacher because he loves getting opportunities to interact with many students, learn things from his students, and teach them as much as he can. 

In his two years here, Mr. He has faced challenges, especially the use of cell phones in class. However, he has found great ways to overcome these challenges. He tries to make his class more engaging for the students and to provide a comfortable environment where no student is neglected. He offers different support systems for each student and personalizes his teaching methods for students' specific needs. 

Mr. He became interested in the field of teaching by participating in many different academic programs, especially working on the student council. Mr. He has always valued the importance of education. He enjoys getting to interact with other teachers and the student body. 

Outside of school, Mr. He enjoys taking photos, specifically landscape photography. Mr. He loves animals, and would rather talk to animals than speak every language in the world! He describes himself as a cat person; he owns two cats! Mr. He also loves to try new things. A recent hobby of his is the drums. He’s also a profound Badminton player, having played throughout college. Mr. He loves movies and watching TV shows on Netflix. His favorite movie is Whiplash, and he loves the TV show House Empty.  He’s also much more comfortable in colder weather conditions. He says, “There are better clothing options and I don’t have to sweat!” 

Mr. He has been here for a mere two years but has accomplished so much as a teacher. His students move on to love the Chinese culture and language. He has forever improved PHS for the better.
