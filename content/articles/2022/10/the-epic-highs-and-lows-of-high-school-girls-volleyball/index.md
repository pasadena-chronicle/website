---
title: The Epic Highs and Lows of High School Girls Volleyball
subtitle: Behind the scenes of PHS's Girls Volleyball team
# A comma seperated list of capitalized names
writers:
  - Gabrielle Trujillo
date: 2022-10-03T15:00:00.000Z
# A comma seperated list of lowercase singular names
topics:
  - sport
---
As people filter into the gym after they pick up their books from their locker, girls volleyball is one of the most popular sports on campus. 3:30 is when the Varsity team plays, so the game is well off as the students find their way to the bleachers at the 3:44 bell. On the outside, you see a flashy, dramatic, over the top game of keep-the-ball-up, but what is it really?

Many girls try out for the team every year, some succeeding and some not. These extensive tryouts are anywhere between 20-40 girls. All of them compete for a spot on one of the volleyball teams: Freshman/Sophomore, Junior Varsity, and Varsity. The Freshman/Sophomore team is coached by Coach Sandra and Coach Meishi, both having coached at PHS for a year. They are led by their team captain Peipei Alexander, an incoming freshman at PHS. She has experience with volleyball, and many of her teammates love her spirit and wisdom with the sport. In a statement, she wrote, “Being on the PHS Frosh Team has introduced me to many different aspects. I love my team - they are my friends, my family, and they always have my back… Of course, with volleyball being tied in, we all have the joy of playing volleyball; uniting us.”

Whether it’s running sprints, doing wall-sits, or scrimmaging, there is never a dull moment. Jadyn Addicott and Tajah Davis, both Sophomores, are the team captains for the JV volleyball team. Jadyn says, “The volleyball team brings us all closer together, and you get to meet a lot of new people and the community is great.” They guide their team through drills, warmups, team huddles, and even on the court. Whether it’s cleaning up footwork or helping out with passing, there’s no doubt that these two are the leaders of the team. Currently, the JV volleyball is sitting at a 10 wins 4 losses record, their league record being 1-1. 

That’s not all - we’ve saved the best for last. Varsity team captain, Kiana Teran, also happens to be their starting libero. At practice, you can hear her calling the balls, leading the team in defensive drills, and powering through conditioning. The Varsity Team is looking forward to their Senior Night on September 29th, where they take on the Muir Mustangs in the Dog House. The Varsity team is full of players who are not only well versed in volleyball, but whose passion shows during practice. Whether it’s going after a ball in a drill, or giving it their all in the game "Queen of the Court", these players’ love for volleyball shows both on and off the court. When on the sidelines, the Bulldogs are chanting and shouting for their team. These girls work hard everyday so they can go head to head with volleyball schools all around SoCal, so you should check out a game if you happen to hear squeaky shoes in the gym!