---
title: The Secret Garden
subtitle: Green Club's New Native Garden is Coming Soon!
# A comma seperated list of capitalized names
writers:
  - Polly Mcconnell
date: 2022-10-03T15:00:00.000Z
thumbnail: img_5634-1-.jpg
# A comma seperated list of lowercase singular names
topics:
  - club
  - student
  - local
  - campus
  - garden
---
Picture this: It’s a beautifully warm, sunny day, and you and your friends are looking for a place to sit down at lunch. You want to breathe in some fresh air, find a patch of shade, or get away from the concrete quads and noise of campus. If only a divine space like this existed at PHS…

Well - it does. 

“The idea in years to come is that it’ll be a place where all students can study and sit during nutrition and lunch,” says Faith Keith, president of PHS’s Green Club. She’s referring to the new and improved native garden that the club has brought to our campus. “Anyone can stop by and check it out!” 

The garden is located in the lot behind the East Quad and Little Theater - look out from the Canteen and you’ll see grapevines, lined rows of vegetables, and a variety of carefully maintained bushes and land masses. 

Up until a couple of months ago, the space was dedicated to growing blueberries, peppers, and other fruits and vegetables. But it wasn’t sustainable, according to Lindsay Frieberger, Green Club’s former co-president. “It’s so inefficient to keep planting plants that *die*,” she said when I talked to her last year. 

Native plants, by definition, require a lot less energy to keep alive because they’re indigenous to the area - it’s their natural habitat. “It’s important because unlike the rest of our garden, it’s drought tolerant - and that’s a really big issue, especially in Southern California,” explains Faith. 

Isa Eisenberg, Green Club’s other co-president of last year, agrees. “We wanted a nice space that provided biodiversity, and an escape from the concrete that we’re surrounded by,” she says. 

So, about midway into last school year, Isa and Lindsay decided to change it up a bit. They set out over winter break with the mission to jumpstart their research as much as possible, touring various schools across the district that implemented native gardens.

They sought out the help of Jesse Chang, executive director of *Catalyst -* a nonprofit organization in San Gabriel Valley that promotes and aids community native garden efforts. With the help of Jesse’s foundation, PUSD master gardener Jill, and Green Club’s weekly garden days, the native garden is up and running. 

The garden hosts an impressive spread of indigenous species like California Buckwheat, wildflowers, and White Sage. Faith paints me the picture of water fountains for both humans and animals, and benches for students to sit on. “The idea is that they can sit there, do homework, or maybe even have some outdoor classes on stumps,” she details, even hinting at an exciting collaboration with the CAMAD Academy to conduct this new style of outdoor learning. 

“Our garden days are mostly the upkeep at this point,” Faith says in reference to pulling weeds and building self-sustaining vegetable beds, the current task for shaping up the garden. According to Faith, Lindsay, and Isa, the fate of the garden depends on the student body’s dedication and support for it. 

Between garden days and continued fundraising through Green Club’s recycling program, the garden will live and grow. “When you show up, and you care, it can be the thing we always wanted it to be.”
