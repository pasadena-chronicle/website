---
title: Applying to College
subtitle: What you need to know
# A comma seperated list of capitalized names
writers:
  - Gwendolyn Lopez
date: 2022-11-01T14:20:00.000Z
# A comma seperated list of lowercase singular names
topics:
  - student
---
For seniors, the month of October means another step closer to the biggest challenge of the school year: applying to college. The process of college applications can certainly seem daunting at first, with all the different components to keep track of. But with skillful time management and focus, you can easily get your application in on time. 

Here’s some basic information that seniors applying to college should be aware of:

* The University of California (UC) application is due by **November 30, 2022.**
* The California State University (CSU) application opened on October 1, and is also due by **November 30, 2022.**
* Enrollment for the summer and fall semesters of 2023 at Pasadena City College (PCC) opens on **December 1, 2022.**
* The FAFSA (Free Application for Federal Student Aid) application opened on October 1, and should be filled out by **March 2, 2023.**
* * Filling out this form is mandatory for graduation.
* Most private universities have their Early Action/Early Decision deadlines on **November 1, 2022**, but regular decision deadlines are generally in **December** and **January.**
* * Check the website of specific institutions for more accurate information.
  * Some private schools require teacher recommendations and/or counselor recommendations—be sure to check the requirements.
* SAT and ACT scores are **not required** for the UC and CSU application in 2022.
* * Many private universities are also going test-optional this year, and you only need to take the SAT if you’re applying to MIT or Georgetown. Check specific institutions for their SAT/ACT policy.
  * Upcoming SAT test dates: November 5 and December 3.
  * Upcoming ACT test dates: October 22 and December 10.

This definitely seems like a lot of work—but for now, most applicants are just focusing on their UC and CSU applications. For the UC application, one of the biggest components is the PIQs, or personal insight questions. Every applicant must pick 4 of the 8 prompts to respond to and use 350 words or less to.

As someone currently working on the PIQs, my advice on approaching them is to write from the heart. It can be difficult to respond to the prompts with the strict word limit and the specificity of the questions, but what colleges are ultimately looking for is a representation of who you are as a person. Don’t be afraid to be genuine and really talk about your passions, struggles, and motivations. And of course, if you ever need help with anything college related, the College Access Plan (CAP) is always available after school on Mondays and Wednesdays in room G102. Additionally, you can email your counselor or visit them in room A111.

Wherever you are in this process, best of luck!

UC application: <https://apply.universityofcalifornia.edu/my-application/login> \
CSU application: <https://www.calstate.edu/apply> \
CommonApp (privates): [www.commonapp.org](http://www.commonapp.org)  \
FAFSA: [www.fafsa.gov](http://www.fafsa.gov)