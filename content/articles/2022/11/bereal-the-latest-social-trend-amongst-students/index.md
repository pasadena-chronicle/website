---
title: "BeReal: The latest social trend amongst students"
# A comma seperated list of capitalized names
writers:
  - Jesus Gonzalez
date: 2022-11-01T12:20:00.000Z
# A comma seperated list of lowercase singular names
topics:
  - local
---
A new trending social media app that actually does more good than harm to its users. **w**hat a shocker. Unless you’ve been living under a rock, you’ve either installed or at the very least heard of the app called BeReal. BeReal was initially released back in December of 2019, but it wasn’t until mid-2022 that it really started gaining popularity.

The way BeReal works is quite simple. In essence, you are tasked with posting a daily photo of yourself within two minutes whenever the app sends you a notification. What makes BeReal different is that all the friends you’ve added to your BeReal also have to post a photo simultaneously. If you or one of your friends post your photo late, the app will send a notification to all your friends that you posted your photo late. Additionally, if you don’t post a photo, BeReal will not let you see your other friends' posts until you post a photo.

While this may all seem a bit extreme for just a social media app, BeReal requires this to fulfill its original purpose. As the name suggests, the entire purpose of the BeReal app is to be real. According to an article titled, "'You’re not glamorizing anything…" published on Protocol on March 29, 2022, and written by Lizzy Lawrence, Alexiss Barreyat was inspired to create BeReal, in part, by the shiny, overly manicured style of influencers. He would see influencer content on social media constantly during a previous job as a video producer for GoPro.

BeReal was made to promote the idea that everyone should be proud of their natural selves and not strive for a version of themselves to appeal to others. **It** accomplishes this by encouraging its users to post pics of themselves at the moment instead of spending several minutes focusing on the best angles, looks, etc. In a time where social media plays such a large role in what we perceive as the ideal look, style, or body type, it’s nice to have apps such as BeReal as a counter-belief that you are perfect as you are.