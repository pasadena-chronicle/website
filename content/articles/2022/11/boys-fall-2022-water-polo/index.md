---
title: "Boys Fall 2022 Water Polo "
# A comma seperated list of capitalized names
writers:
  - Xida Hu
date: 2022-11-01T18:20:00.000Z
thumbnail: waterpolo.jpg
# A comma seperated list of lowercase singular names
topics:
  - sport
  - student
---
As of the writing of this article, the PHS Boys water polo team has yet to win a game this season. However, despite the perhaps inevitable loss of morale looming in front of the players as each and every day passes, the team has never been more willing to put in the effort towards improving their team chemistry and working towards their first win. 

The Fall of 2022 has been a season of change. With the introduction of a new head coach, McKenna Imset, the team has had a breath of fresh air, and has been introduced to a new and refreshing coaching style. It did not take long for the team and Coach McKenna to start getting along and start forming better team chemistry, which was necessary as the preseason had been cut short and the first game was coming up in a few days. 

As the team's first game came to a close, the scoreline was 9-4. Not a great start to the season, but given the circumstances as a whole, it wasn’t a bad performance altogether either. As the season played out, and more and more games came to an end, the boys began to gain further experience playing together and were able to create more coherent team play.

Towards the end of the season, Senior Night was coming around. Despite some setbacks that the team encountered regarding playing a home game, they pulled through and in the end played a really close match, ending 16-18 in the favor of Muir, the guest team. 

The 2022 season of water polo brought about healthy changes that many of the players did not expect, and much less expected to improve the overall situation regarding water polo at PHS. Although the team did not take home any wins this season, the experience as a whole was a great learning process for all the players, and the players themselves are sure to look back on this season and treat it as a special one.

Photo of the Varsity Water Polo "Tuna Tussle" Senior Night. Taken by Ajorin Ashing-Giwa.