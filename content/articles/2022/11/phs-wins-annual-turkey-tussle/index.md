---
title: PHS Wins Annual Turkey Tussle
subtitle: Homecoming Week 2022
# A comma seperated list of capitalized names
writers:
  - Mallika Sheshadri
date: 2022-11-01T19:20:00.000Z
thumbnail: pasadenachronicleoctober.png
# A comma seperated list of lowercase singular names
topics:
  - event
---
This year, Pasadena High saw lots of school pride in this year’s Homecoming Spirit Week leading up to the Turkey Tussle. Annually held at the Rose Bowl, a 92,542-seat football stadium and National Historic Landmark, our Homecoming game is one of the highlights of students’ academic years. This year, the Bulldogs made their school proud, breaking out of their losing streak with a score of 47-0!

This rivalry has been a defining feature of the Turkey Tussle since its inception in 1947. At the time, the game was played between Pasadena Community College and John Muir Community College, institutions that would eventually evolve into the high schools we attend today, alongside other descendants.

The Victory Bell, however, is not as old as the event itself, as it was introduced in 1955. The Bell, used initially on a steam locomotive, was introduced by Associated Student Body Vice-President Jim Shelton, whose father was General Manager of the Santa Fe Railroad Company. As a rotating trophy between Pasadena and Muir, the Victory Bell is symbolic of the regular culmination of the school’s competition.

Since last year’s game was canceled due to COVID, this year’s game was especially anticipated. The Tussle saw tons of students filling the stands and great participation from students in Spirit Week activities. Spirit Week, characterized by 5 thematic days, included Marvel/Disney Day, Duo Day, Pajama Day, the traditional Muir Nerd Day, and Red and White day. 

Students also participated in the annual Campus Deck, spending weeks preparing cardboard cutouts and plywood constructions that decorated the school grounds last week. All the classes made setups to be proud of, but better than all of them were the seniors, the class of 2023, who won first place.

Last Friday, many Pasadena High School students celebrated this victory at the 2022 Homecoming Dance, set to take place on October 28th at Santa Anita Park. The Homecoming Court is composed of seven princesses: Lily Vargas, Cheska Braganza, Brianna Tirado, Parami De Silva, Vega Benn, Sophia Covarrubias, and Raela Jayasekara. Raela was crowned Queen at the Homecoming Assembly on Wednesday, October 26th, which saw lively performances from cheer, orchesis, and the drum core.

Very few high schools possess a homecoming game with such a tradition, especially one played in a professional football stadium. As Bulldogs, we have much to be proud of and thankful for. Let’s go Bulldogs!