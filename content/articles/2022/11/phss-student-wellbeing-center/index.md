---
title: PHS's Student Wellbeing Center
subtitle: A welcome, confidential space for all students!
# A comma seperated list of capitalized names
writers:
  - Polly Mcconnell
date: 2022-11-01T15:20:00.000Z
thumbnail: img-6506.jpg
# A comma seperated list of lowercase singular names
topics:
  - student
  - local
---
As students are finally getting settled into a rhythm with our new school year, many have noticed and taken advantage of the up-and-running Student Wellbeing Center. This humble and welcoming space, located in G202, is open Monday, Wednesday, and Friday until 4 pm for every student at PHS. 

Brittani, one of the two Youth Educators I spoke with, explains the purpose of this new addition to our campus. “It’s a safe space for students to focus on mental health, sexual health, and the consequences of substance abuse,” Brittani says. 

She and her desk partner Karineh, both county-employed residents in the room, are trained to a master level in such topics. In the Wellbeing Center, confidentiality is a *top priority.* Being outside workers, as opposed to on-site PHS employees, Karineh explains to me, adds a level of comfort for a lot of students coming in. “If there’s an issue that a student’s having with a teacher, or with the school, they feel safe sharing in here. We’re the only adults allowed in.” 

And yet, a safe space is not theonly service that the Wellbeing Center offers. Walking in, you’ll notice an ‘Anonymous Questions’ box, information about mental or reproductive health institutions, and even direct resources like condoms, tampons, and pads. You can also find a referral there to mental and reproductive health institutions.

If you’re interested in becoming more involved with the mental health of those on our campus, you might want to consider the center’s Peer Advocate program. You can receive community service hours and letters of recommendation for taking part. 

The room itself features calming music, positive affirmations and role models on the wall, stress toys, games, snacks, water, and comfortable couches and chairs - all of which create an atmosphere that promotes a clear mind, and a break from the outside world. 

“Some come in to take a nap, and some just need a moment to themselves,” Karineh explains to me. “I think it’s important for students to feel like they’re heard.” 

As adolescents going through what most consider to be one of the toughest times of our lives, it’s *crucial* that we check in with ourselves every once in a while. Feeling stressed, alarmingly busy, or alone are all valid feelings and emotions that we must allow ourselves to feel - and more importantly, recognize. 

The Wellbeing Center is open to you, me, and anyone at our school going through a rough time. If you know a friend who needs someone to talk to or even just a moment away from the often overwhelming storm of classes and social life, walk them over to the Wellbeing Center for the afternoon. Everyone is welcome.