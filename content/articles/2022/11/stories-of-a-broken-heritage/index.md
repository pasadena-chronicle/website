---
title: Stories of a Broken Heritage
subtitle: Hispanic Heritage Month
# A comma seperated list of capitalized names
writers:
  - Gabrielle Trujillo
date: 2022-11-01T15:20:00.000Z
# A comma seperated list of lowercase singular names
topics:
  - local
---
As the end of September rolled around, Latino posts surged in Instagram and Tiktok feeds. With a google search, students learned this was the start of Hispanic Heritage Month, a crucial time for some, and irrelevant to others. So what exactly is it? National Hispanic Heritage Month spans 31 days, from September 15 to October 15 annually, in which it celebrates the histories and cultures of peoples whose ancestors came from Spain, Mexico, the Caribbean, and other Spanish-speaking countries. With that, here are a few stories of my heritage.

I am Mexican, a Mexican who doesn’t know how to speak proper Spanish. A Mexican put in the regular Spanish class, with non-native speakers. A Mexican who can struggle to identify with her culture. My grandfather grew up in El Paso Texas, which at the time was the most racist place in the United States. He was forced to speak English at school, where he was mocked for his Mexican accent, and came home to his traditional Mexican family, where he was forced to speak Spanish. Throughout his life, he faced unacknowledged adversity for being Mexican, and it’s today that he realized it.

My grandfather is fair-skinned– which meant he could get away with a lot. He could sit in “white only” sections, walk on “white only” sidewalks, and could even get away with passing as a white man. My grandfather’s name is Joseph Jauregui (How-rre-gee), a name that would go on to be completely mispronounced every day of his life. I have the privilege to listen to his many stories and many interactions with racism.

My grandfather was drafted into the army. When the drill sergeant was taking roll, instead of using his last name, the drill sergeant addressed my grandfather as “jaguar” or “alphabet”-- because my grandfather had “every letter in the alphabet in his name”. In the army, the social groups were naturally segregated, as my grandfather said; the normalized racism during his time was sickening. 

When my grandfather wanted to become a policeman, he was interviewed more than a year after he applied. Upon showing up, the white sheriff said “We need to have more of your kind in our department.” The sheriff was referencing that they didn’t have enough Mexicans in the staff, and that was the only reason he was being interviewed. My grandfather proceeded to walk out, and didn’t get the job.

When my grandfather got a position on the school board, he was surrounded by white people. By this time, he had dropped the accent and adjusted to the norms of society. When talking with friends, they would bash Mexicans shamelessly. When he would then look at his “friends” they’d realize what they had just said, and would follow up with, “Oh, but you’re not like that Joe, you’re different.”

These many interactions led my grandfather to not teach Spanish to his kids, in fear they would go through the same hardship. This trend of assimilation, of people who are desperate to fit into a society that views them as less than, led to a heritage and this month reminded me of how we all must find our heritage.