---
title: Teacher Spotlight - Dr. Anne Rardin
subtitle: The Chronicle sits down for an interview with PHS's orchestra director.
# A comma seperated list of capitalized names
writers:
  - Jasmine Sov
date: 2022-11-01T16:20:00.000Z
thumbnail: pasadenachronicleoctober.png
# A comma seperated list of lowercase singular names
topics:
  - teacher
---
From banging on pianos at age two, starting the violin at seven, and earning her doctorate in music education, Dr. Anne Rardin has music running in her blood. “My great-grandfather used to play for square dances on the fiddle and for silent movies,” she says with pride. Now in her fourth year as director of PHS’s orchestra program, Dr. R has contributed nothing short of excellence to the orchestra. 

During my interview with her, she leans forward eagerly as she discloses the following: “I was at the Eastman School of Music, and there were all these players that were really snooty about *I’m only going to be a performer. I’m never going to teach.* And yet, I saw that this was killing classical music.” Dr. R entered music education for two reasons: she enjoyed music, and she also enjoyed working with young people. She sought to be “a performer who was really interested in music education and an educator who was really interested in performing.” For the sake of keeping classical music alive, she wanted to get more young people interested.

When Dr. R first arrived at PHS in 2019, the music program had a very low number of students. “I had two students in one class and four students in the other class, and there were only two classes,” she says. “And so I started sending them out, saying, ‘Help me build this program back. Help me find everyone that used to be in orchestra.’” And within a couple of months, Dr. R’s efforts paid off—six students became 25. This year, the orchestra’s incoming freshman class is the largest they’ve ever had.

How does she choose music for her students to play? It’s a hard decision: “There is so. Much. Great. Music.” As I listen to Dr. R talk about all the different kinds of music that exist, I can hear the love in her voice. “I try every year to pick some favorites…I try to pick music that I know my beginners can do but will also interest my advanced players because we’ve got people that have one year of experience or less all the way to 12 years of experience.”

I ask Dr. R what the most rewarding part of teaching music is for her. “Well, the cute answer is all of it, because I really, really, really love what I get to do.” She chuckles. “I guess \[it’s] when I see the light bulb go on, and somebody is able to make a change…and all of a sudden it’s easier, and they can really make music. And I mean like *really* make music.”

So what does Dr. R want PHS to know about the orchestra? “There are some really talented people here!... It’s like a hidden gem nobody knows about!” It’s true that the orchestra was once nearly unknown to the student body of PHS. But thanks to Dr. R, we can be sure that this talented group of musicians will continue to play for many years more.