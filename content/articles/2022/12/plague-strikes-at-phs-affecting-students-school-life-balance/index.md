---
title: Plague Strikes at PHS, Affecting Students' School-Life Balance
subtitle: The surge is leaving lasting impacts on students' academic performance.
# A comma seperated list of capitalized names
writers:
  - Jasmine Sov
date: 2022-11-08T15:00:00.000Z
# A comma seperated list of lowercase singular names
topics:
  - student
  - local
---
As the holiday season approaches, so too does the seasonal plague. This year’s flu season has struck our school with a vengeance, surging its way through the student population. Many are out with influenza and the common cold—however, the presence of COVID-19 has taken out many more. In the past few weeks, students have missed key days of instruction while attempting to recover at home.

Due to the surge, students who were out with illness have had to balance catching up on schoolwork, making up midterm exams, and managing their own mental well-being. Several students I interviewed found it difficult to catch up with work after they got back from being out sick. “I felt like I fell behind in all of my classes,” said one student, who was out for two weeks with the flu and bronchitis. “I considered going to school even when I was very sick because I didn't want to miss the 4th test that week.” 

Others who were out for a shorter period of time suffered similar consequences. “I'm still dealing with repercussions from only missing three school days…I was literally crying the day I had to make up a bio test because I was stressing that a 30-minute advisory wasn't enough \[time to take the test],” stated one junior. 

The spike in illness comes as the countdown to final exams this semester begins, which adds another dash of stress to the mix. Moreover, every grade has its difficulties: seniors are scrambling to submit college applications, juniors are trying to keep their grades in the clear in the “most important year for college”, sophomores are adapting to an increased workload, and freshmen are still adjusting to the high school climate.

One student nicely summarized the issue: “Missing school has definitely been a huge mental drain. There’s just so much work to catch up on that it’s kinda hard even to get started. Also, I was pretty tired when I was sick, so I couldn’t really keep up while at home." 

“Add on to that a bunch of tests,” they said, “and there was just so much.”

Here’s how you can protect yourself from this year’s seasonal plague, as recommended by the CDC:

* Avoid close contact with sick people.
* If you are feeling sick, stay home and take a COVID-19 test.
* Cover your mouth and nose when sneezing or coughing.
* Wash your hands often with soap and water.
* Avoid touching your eyes, nose, and mouth.
* Stay up to date with COVID-19 vaccines.

<https://www.cdc.gov/coronavirus/2019-ncov/prevent-getting-sick/prevention.html> 

<https://www.cdc.gov/flu/prevent/actions-prevent-flu.htm>