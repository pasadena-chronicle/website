---
title: "Teacher Spotlight: Mr. Chen"
subtitle: The most beloved teacher on campus
# A comma seperated list of capitalized names
writers:
  - Mallika Sheshadri
date: 2022-11-08T14:59:00.000Z
thumbnail: mrchen.jpg
# A comma seperated list of lowercase singular names
topics:
  - teacher
---
There are a lot of teachers at Pasadena High School, so many so that **maybe change this to "in fact,"** no one student **seems to** know every teacher. But there is **one** teacher that everyone knows — Mr. Chen, a **beloved** substitute teacher with a smile that stretches for miles. Mr. Chen was a technician who worked on railroads in China until he emigrated to California, where he got a degree in Mechanical Engineering at PCC and **later** CalState LA. He worked as an engineer in Sierra Madre for many years until his love for trying new things led him to the world of education.

Mr. Chen considers himself a student forever - he always wants to learn new things. That's why he was so drawn to education. “It’s different from my experience in the industry. I have always liked tangible things that you can hold, but education is different,” he tells me as we sit down in the back of Mr. Lambert’s classroom. “ You can only hope that you made a difference.” 

He got into education in 2010-2011 as a substitute teacher for elementary schoolers. “It was tough! I had no experience. I was scared,” he reminisces with a chuckle. He continued to take substitute jobs around PUSD until 2016 when the district offered him a long-term substitute position at Marshall Fundamental High School. “They wanted me to teach 8th-grade math! I love math. I have always had a passion for geometry,” he says, animatedly. “I had been out of school for many years. I came back and the curriculum was so different! In college, you learn the principles and the foundations. Those stay the same. So, \[in order to teach the students effectively],I would learn the new things- from the book, online, and I would teach them.” Mr. Chen stayed at Marshall for those few months, but PHS was always a goal for him.

 “PHS, it’s only for substitute teachers with seniority**.** In 2017, I started getting PHS jobs, and I took them! Come 2018, I was here full-time. I love PHS! The people here are so wonderful, and I’m always learning new things.”

These things he learns from people around him, I find, are what Mr. Chen values the most as a teacher. “I’m so grateful, for everything. The math teachers here, and at Marshall, are my idols, my mentors. Learning from them is like being a kid in a candy shop! And learning what works for a certain group of kids, I love that. The administration, the teachers, everyone here is so kind, and I learn so much from them and the students!”

Mr. Chen is also very kind to everyone around him, I included. The first time I met Mr. Chen was when he was my substitute teacher in PE my freshman year. My name, Mallika, is not an easy one to pronounce, but when calling attendance, Mr. Chen got pretty close; something that rarely happens with substitute teachers. I went up to him after he called role and told him that teachers rarely pronounced my name correctly. From then on, he made a point of calling me by my name. In hallways, classrooms, and around campus, Mr. Chen always says hello.