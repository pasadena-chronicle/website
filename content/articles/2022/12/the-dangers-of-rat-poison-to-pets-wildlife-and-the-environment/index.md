---
title: The Dangers of Rat Poison to Pets, Wildlife, and the Environment
subtitle: Understanding the risks and alternative solutions to controlling
  rodent infestations
# A comma seperated list of capitalized names
writers:
  - Girl Scouts Troop 1551
date: 2022-12-22T17:49:22.013Z
thumbnail: dall·e-2022-12-22-09.56.43-a-graphic-design-cover-for-an-article-about-the-danger-of-rat-posion.png
---
Everyone loves their pets, and we all want what is best for them. Unfortunately, accidents happen, and things we think would help our everyday life can end up hurting our furry friends. Rat poison is one of those everyday items that are harmful to our pets, but it is usually overlooked when it comes to its impacts. Using rat poison not only kills rats but also can cause harm to your pets and local wildlife who may consume the rats or the poison itself. 

The earliest rat poisons used heavy metals, such as arsenic and thallium. These poisons were phased out during the 40s and replaced with anticoagulants (blood thinners) such as warfarin, which was introduced in 1947. Warfarin, along with chlorophacinone and diphacinone, is a first-generation rat poison. One key feature of these anticoagulants is that it takes multiple doses of the poison: a rat must feed at multiple baits, or the same bait multiple times, in order to be killed. This is not true of second-generation anticoagulants, which can kill with a single dose ingested.

Second-generation anticoagulants, or SGARs, were developed in the 1970’s and include brodifacoum, bromadiolone, and difethialone. These rodenticides are not easily excreted and persist within a rat’s organs. For this reason, they are more dangerous to wildlife that consume rats than first-generation rodenticides. While most rodenticides are anticoagulants, there are other types of rat poison on the market, such as bromethalin and zinc phosphide. Bromethalin is a neurotoxin that causes respiratory failure in the rats – and any wildlife or pets that ingest a lethal dose. Zinc phosphide releases phosphine gas when it reacts with stomach acid, and can be lethal in a matter of hours. 

Though these poisons target mice and rats, other animals higher up the food chain also suffer the consequences through primary or secondary poisoning. Primary poisoning occurs because the rodenticides have ingredients or flavors that make them attractive to not only rats but also nearby wildlife. Predators are also vulnerable to secondary poisoning when they eat rats or other prey already afflicted by the rodenticide and then also become ill. 

A study in 2018 done by the California Department of Pesticide Regulation found that among the most affected were mountain lions, bobcats, hawks, and other endangered wildlife like Pacific fishers, spotted owls, and San Joaquin kit foxes. Among the mountain lions tested, just over 90% of them tested positive for some kind of rat poison. 

All in all, while unwanted rodents can cause problems, you shouldn’t use rat poison. There are many other options to deter rats from your house without dangerous environmental impacts. To discourage rats from gathering in or near houses, remove low-growing vegetation and collect or dispose of nearby fallen fruit. Checking for cracks or crevices in your house and sealing them up is another way to keep rodents away. You should also seal any food products that end up outside, especially those in trash cans. If the rat problem persists, there are also more humane traps that can be used instead of rodenticides, such as snap traps or electric traps. Promoting natural predators like owls by using or building owl boxes is another effective method of rat control. Owl boxes are installed in nearby trees, and offer a home to the owls who at night hunt for prey like rats. 

Rat poison is an easy solution to a rodent problem, but overall, it’s a danger to the environment, as well as pets and small children. Moving away from the use of rat poison is healthier for you and the animals around you. There are many alternatives, several of which are outlined above. Please try to make use of some of them!