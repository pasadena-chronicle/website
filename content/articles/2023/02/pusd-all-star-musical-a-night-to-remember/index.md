---
title: "PUSD All-Star Musical: A Night to Remember"
# A comma seperated list of capitalized names
writers:
  - " Mallika Sheshadri "
date: 2023-02-27T17:15:11.489Z
thumbnail: cover.webp
# A comma seperated list of lowercase singular names
topics:
  - events
---
On January 26th and 27th, thousands of Pasadena community members gathered at the Civic Auditorium to watch the product of hundreds of students’ tireless efforts, at the first-ever PUSD All-Star Musical in collaboration with the Pasadena Playhouse. Students from all high schools came together on stage, in the pit, and in the wings, in this production of Steven Sondheim’s \_Into the Woods. \_

The show had three runs; two matinees reserved for PUSD students and a Friday night show for the general public. That evening, I entered the Civic auditorium, admiring the ornately decorated ceilings, utterly unprepared for what was to come. The lights dimmed, and the sound of the orchestra tuning filled the theatre. The curtains parted, and the narrator began.

All actors filled their roles to the highest capacity, capturing all the emotions and humor that fill Sondheim’s masterpiece. Yazlynn Acosta was sweet, charming, and humorous as Cinderella. Jack, played by Mattias Orr, brought power to the boyish exuberance of his character. Little Red Riding Hood, played by Madeline Harbison, was cute, sweet, and mischievous. The Baker, portrayed by Asher Hammer, embodied every emotion his character felt, as he stepped out of his comfort zone in the woods. The Baker’s wife, played by Makenna Magalong, was a joyful and comedic presence on stage as her character transitioned from the perfect wife to a mischievous person tormented by her morals. Most stunningly was the witch, played by Ida Hartel, who had an unforgettable voice that was raw, powerful, and at times, completely heartbreaking.

From the opening number to the final bow, every character maintained the same energy, never seeming to falter. The costumes, designed by John Muir high school students, were immaculate and creative. The crew ensured every transition was seamless, and the live orchestra enhanced the atmosphere. I am so proud and appreciative of the hard work and dedication students put into the production, and I can only hope that the All-Star musical becomes an annual tradition.