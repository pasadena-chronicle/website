---
title: "Teacher Spotlight: Mrs. Beck"
subtitle: Making calculus a little easier for all of us!
# A comma seperated list of capitalized names
writers:
  - Polly McConnell
date: 2023-02-27T17:09:39.554Z
# A comma seperated list of lowercase singular names
topics:
  - teachers
---
It’s safe to say that students are terrified by the word calculus. advanced math courses, especially those prefixed with the daunting “AP”, seem to invoke a quarry of emotions like stress, fear, or wonder for most - this is a level of math that a lot of students don’t even wish to tackle in college.

Fortunately for all of us at PHS, our AB and BC Calculus teacher is set on changing the stigma around her subject. “A lot of people think of it as a list of rules, or procedures, that you have to have a really good memory to understand,” muses Mrs. Beck, here on her 7th year teaching at Pasadena High. Also one of the Math 3 teachers this year, she has various other courses under her belt.

Mrs. Beck received her Bachelor’s degree from UC Irvine, with a major in math and a minor in education. She also achieved her Master’s degree at Cal Poly Pomona.

“Math was always my favorite subject,” she explains. “Going as far back as I can remember, even as a little kid, I loved patterns and numbers. I loved making all the connections.” After having discovered an interest in calculus and physics in high school, she decided to pursue a degree in mechanical engineering.

The classes were interesting enough, but as Mrs. Beck tells me, she felt that there was a disconnect somewhere. So, in search of a new path, she took a year off. “I actually interned with the church that I was going to at the time, and I was doing curriculum for the kids church - and, well, I really liked it.”

The next year, she went back to school, figuring that she could combine this new passion in education with her love for math. Right in time, she found a program at UC Irvine that offered a hundred dollars to anyone looking to pursue math education… and the rest was history.

During college, Mrs. Beck’s hobbies included sewing, thrifting, and crafting. Now, she finds joy in the challenge of getting more buy-in from students who aren’t initially interested in calculus. “I always love when students are into the math,” she says. “My favorite thing is when a person who doesn’t even plan on pursuing math has a better appreciation for it after the class.”

It’s no wonder that Mrs. Beck’s door is always open. “She goes out of her way to help her students,” said Jasper Barrera, a junior taking her Calculus AB class. “I remember staying after school one day and just asking her question after question… I was terrified, but she didn’t hesitate to help me out!”

If you haven’t already, you can definitely look forward to taking these higher math classes with Mrs. Beck - she makes calculus a little less scary for all of us.