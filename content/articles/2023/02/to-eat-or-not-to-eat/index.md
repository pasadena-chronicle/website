---
title: To Eat or Not to Eat?
subtitle: PHS students and teachers clash over the right to snack
# A comma seperated list of capitalized names
writers:
  - "Polly McConnell "
date: 2023-02-27T17:12:39.073Z
thumbnail: cover.webp
---
Tuesday afternoon. Sixth period. 2:15pm. Math. Your worksheet? Forgotten. Your stomach? Empty. The person to your right pops open a bag of Takis, and the mouthwatering smell of the spicy, crunchy snack wafts straight to your seat.

What is the only thing you’re thinking right now?

“I want some,” sophomore Judah Alvaro replies, offering an answer that we can certainly all agree on.

Snacking in class is a widely contended issue at PHS. It seems as though students and teachers are constantly at war over the right to eat. Even tiny semantics - like what foods are and aren’t allowed, how they can be eaten, and what periods they may be eaten in - are bones of contention.

I began my inquiry with Mr. Michealsen, who is determined to keep his classroom snack-free. But believe it or not, his motive isn’t to deprive students of joy—his banishment of food perhaps comes from a more understanding place.

“If you’re eating, and someone next to you is really hungry…well, that sucks,” he explains. Michealsen believes that the main issue with eating in class is jealousy and distraction…and, of course, the mess that it leaves.

Sure enough, crumbs and wrappers are the culprit in Ms. Gomez’s classroom. She’s recently embarked on a mission to curb the eating in her classroom for this very reason.

“The building is very old, so sometimes a bunch of critters and rodents, especially ants, tend to eat the sugary things,” she laments, gesturing to her food-riddled floor. As it turns out,the cleaning schedule of the custodians is frequently changed around, so not all parts of the school end up getting cleaned every day.

Dr. Kodama offers another potential concern regarding eating in class: “In the midst of a global pandemic, it could be problematic,” he speculates. “It’s not exactly against our code of health, but you do have to be mindful.”

That being said, our principal finds it highly encouraging that people are comfortable enough to eat in class, and fully supports the endeavor. In Dr. Kodama’s words, “Food represents nurturing and community.”

His opinion is shared by the student body, which unanimously affirm Michealsen’s point about the distraction paradox. For sophomore Mark Lytle, however, food is even more distracting when it isn’t in the room. Dr. Rosa is one teacher who understands this pain.

“People are hungry. Snacks make people happy, and happy people are easier to teach,” Rosa explains. This attitude is much appreciated by his students.

“That class is snack heaven,” Mark preaches.

At the end of the day, eating in class seems to be helpful rather than harmful for the focus and wellbeing of students. It also goes without saying that we should be held accountable for cleaning up after ourselves when we do decide to eat. As long as food doesn’t obstruct the core purpose of school—learning—there should be no problem.